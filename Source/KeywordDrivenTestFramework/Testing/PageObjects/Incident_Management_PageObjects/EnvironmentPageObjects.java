/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

/**
 *
 * @author SJonck
 */
public class EnvironmentPageObjects {

    public static String EnvironmentTabXPath() {
        return "//div[@id='control_BC138B8D-FA48-4352-A20A-7FB5AF468D6C']//div[text()='Environment']";
    }
    
    public static String envirSpillRecordNumXpath(){
        return "//div[@id='form_225A3932-61B8-4E84-97AD-535E23855E13']//div[@id='divPager'][@class='recnum']/div[@class='record']";
    }

    public static String MonitoringRequiredCheckboxXPath() {
        return "//div[@id='control_9AD21B46-7462-4311-9D26-146E3DC51234']//div[@class='c-chk']";
    }

    public static String UncontrolledReleaseOrSpillXPath() {
        return "//div[@id='control_CEB22C22-C683-43D8-90E7-7EE38805727C']//span[text()='Uncontrolled release / spill']";
    }

    public static String environmentalPillGrip() {
        return "//div[@sourceid='225A3932-61B8-4E84-97AD-535E23855E13']";
    }

    public static String UncontrolledReleaseOrSpillAddButtonXPath() {
        return "//div[@id='control_CD5D42EA-CA49-4D1F-A1AB-6E0C42C3AF80']//div[@title='Add']";
    }

    public static String ProductDropdownXPath() {
        return "//div[@id='control_4D0FAC22-8D64-4C4A-85B7-21D05D42043E']//li";
    }

    public static String QualityReleasedTextFieldXPath() {
        return "//div[@id='control_A2598D8A-BBDF-4A8B-A667-E709942B8DE3']//div//div/input";
    }

    public static String evironmentSpillFlowProcessButton() {
        return "//div[@id='btnProcessFlow_form_225A3932-61B8-4E84-97AD-535E23855E13']";
    }

    public static String evironSpillAddPhaseXpath() {
        return "//div[text()='Add phase']//../..//div[@class='step active']";
    }

    public static String evironSpillEditPhaseXpath() {
        return "//div[text()='Edit phase']//../..//div[@class='step active']";
    }

    public static String QualityContainedTextFieldXPath() {
        return "//div[@id='control_11728820-4C02-4C13-B2BF-216C069EECB4']//div//div/input";
    }

    public static String UnitOFMeasureDropdownXPath() {
        return "//div[@id='control_DBC9A52C-ACE5-43D2-A8EB-BA8243A6E73A']//li";
    }

    public static String DetailedDescriptionTextAreaXPath() {
        return "//div[@id='control_F84B27C2-67B3-4226-95E5-3D3C09FCB91A']//textarea";
    }

    public static String CleanUpCostTextFieldXPath() {
        return "//div[@id='control_822E9E45-A2A6-4136-91F5-DEBCC42C42CC']//div//div//input";
    }

    public static String SubmitButtonXPath() {
        return "//div[@id='control_D25F3D20-8E33-419C-B724-EE84BEAC65CA']//div[text()='Submit']";
    }
    
    public static String validateEnvirSpillgridSavedRecord(String record){
        return "//span[text()='"+record+"']";
    }

    public static String evironSpillSaveButtonXpath() {
        return "//div[@id='btnSave_form_225A3932-61B8-4E84-97AD-535E23855E13']";
    }

    public static String ValidateRecordWasCreatedXPath() {
        return "//div[@id='control_CD5D42EA-CA49-4D1F-A1AB-6E0C42C3AF80']//tr//div[text()='Acetone']";
    }

    public static String MonitoringHeadingXPath() {
        return "//span[text()='Monitoring']";
    }

    public static String SelectAllButtonMonitoringAreasXPath() {
        return "//div[@id='control_0FA48973-4517-449F-88FE-5484E372B9FE']//b[@class='select3-all']";
    }

    public static String WaterTabXPath() {
        return "//div[text()='Water']";
    }

    public static String ValidateWaterValuesXPath() {
        return "//div[@id='control_00278E07-DE4B-4ED8-8A29-41A4557D137C']//table[@data-role='selectable']";
    }

    public static String AirTabXPath() {
        return "//div[text()='Air']";
    }

    public static String ValidateAirValuesXPath() {
        return "//div[@id='control_942C9A46-36E7-4872-A014-1661890599BF']//table[@data-role='selectable']";
    }

    public static String QualitySubTab() {
        return "//div[text()='Quality']";
    }

    public static String QualityConcessionsXPath() {
        return "//span[text()='Quality Concessions ']";
    }

    public static String ConcessionsAddButtonXPath() {
        return "//div[text()='Concession']/..//div[text()='Add']";
    }

    public static String ConcessionDescriptionTextAreaXPath() {
        return "//div[@id='control_04842A6A-3005-4D07-9131-6B25C1373134']//textarea";
    }

    public static String ImpactTextAreaXPath() {
        return "//div[@id='control_9ECE066A-3D4C-4209-B2A3-54ED4D4DF4E8']//textarea";
    }

    public static String PartiesAffectedDropdownXPath() {
        return "";
    }

    public static String CorrectiveActionTextAreaXPath() {
        return "//div[@id='control_83CBB14B-6E16-490E-B408-EF19607B33AD']//textarea";
    }

}
