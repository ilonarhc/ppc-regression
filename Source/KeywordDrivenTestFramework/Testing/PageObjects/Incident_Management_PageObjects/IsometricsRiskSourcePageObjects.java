/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author Ethiene
 */
public class IsometricsRiskSourcePageObjects {

    public static String riskSourceXpath(String riskSource) {
        return "//div[@id='grid']/div[3]/table//tr//td[4]/div[text()='" + riskSource + " ']";
    }

    public static String riskSourceCheck() {
        return "//div[@id='control_0CA5B12B-E073-44E3-ADB4-324B4048410A']";
    }

    public static String riskAssRefXpath() {
        return "//div[@id='control_149AD1FD-AE44-44DA-A64F-E37A03D9B491']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String riskDesciptionXpath() {
        return "//div[@id='control_8C191992-EC11-43A5-B2E9-2AD727C55B1C']//textarea";
    }
    
     public static String rmBusinessUnitXpath(String name) {
        return "//div[@id='formWrapper_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']//a[text()='"+name+"']//../i[@class='jstree-icon jstree-ocl']";
    }
     
     public static String riskOwnerDropdownXpath(){
         return "//div[@id='control_C57075CF-1363-4C90-BE35-4D0785DB5FED']";
     }
     
     public static String rmBusinessUnitCheckXpath(String name) {
        return "//div[@id='formWrapper_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']//a[text()='"+name+"']//../i[@class='jstree-icon jstree-checkbox']";
    }
     
     public static String riskMatrix(){
         return "//div[@id='control_F24083F5-96D3-4FA2-B288-0B55829E0AF4']";
     }
     
      public static String inherenrRiskButtonXpath(){
         return "//div[@id='control_EBB23E50-7822-4700-91E5-177849D89B4F']";
     }
      
      public static String addImpactRiskAddButton(){
          return "//div[@id='control_EBB23E50-7822-4700-91E5-177849D89B4F']//div[text()='Add']";
      }
      
      public static String riskOwnerXpath(String name){
          return "//div[contains(@class,'transition visible')]//a[text()='"+name+"']";
      }
      
      public static String impactTypeDropdownXpath(int num){
          return "//span[text()='Inherent Risk']//../..//div[1]/div[2]/div[2]//tr["+num+"]//td[4]";
      }
      
       public static String impactDropdownXpath(int num){
          return "//span[text()='Inherent Risk']//../..//div[1]/div[2]/div[2]//tr["+num+"]//td[5]";
      }
       
       public static String likeliHoodRatingDropdownXpath(int num){
          return "//span[text()='Inherent Risk']//../..//div[1]/div[2]/div[2]//tr["+num+"]//td[6]";
      }
       
      public static String selectFromDropdown(String option){
          return "//div[contains(@class, 'transition visible')]//a[text()='"+option+"']";
      }
      
      public static String selectFromDropdownInside(String option){
          return "//div[contains(@class, 'transition visible')]//a[text()='"+option+"']//../i[@class='jstree-icon jstree-ocl']";
      }
      
      public static String inherentSaveButtonXpath(){
          return "//div[@id='control_EBB23E50-7822-4700-91E5-177849D89B4F']//div[text()='Save']";
      }
      
       public static String editButtonOfImpactTypeXpath(int num){
          return "//span[text()='Inherent Risk']//../..//div[1]/div[2]/div[2]//tr["+num+"]//td[2]";
      }
      
      
      
      //div[@id='formWrapper_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']//div[contains(@class,'visible select3-drop')]
}
