/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

/**
 *
 * @author SJonck
 */
public class InjuredPersonsPageObjects {

    public static String whyAnalysisPanel() {
        return "//span[text()='Why Analysis']";
    }
    
    public static String whyAnalysisPanelAdd() {
        return "//div[text()='Why Analysis']/../..//div[@id='btnAddNew']";
    }

    public static String injuredPersonsSubTabXPath() {
        return "//span[text()='Injured Persons']";
    }
    
    public static String whyAnalysisOrderNumber(){
        return "//div[@id='control_7831FA38-FB43-46A4-893D-42D365D66A05']";
    }
    
    public static String whyAnalysisWhy(){
        return "//div[@id='control_520640B0-0730-4F01-9D34-D185B8CCDA27']";
    }
    public static String whyAnalysisAnswer(){
        return "//div[@id='control_5A999D23-9FE7-4E9D-90E5-8A467467AB6A']";
    }


    public static String saveAndColse() {
        return "//div[@id='control_C2C40A28-FCC3-4B4D-BCB2-77F4467A3920']//div[text()='Save and close']";
    }

    public static String saveXpath() {
        return "//div[@id='btnSave_form_D6F2F0CD-C202-4238-92BF-3A91A2B306D7']//div[text()='Save']";
    }

    public static String injuryEditphaseXpath() {
        return "//div[@id='divProcess_D6F2F0CD-C202-4238-92BF-3A91A2B306D7']//div[@class='step active']//div[text()='Edit phase']";
    }

    public static String injuredPersonProcessButtonXpath() {
        return "//div[@id='btnProcessFlow_form_D6F2F0CD-C202-4238-92BF-3A91A2B306D7']";
    }

    public static String injuredPersonseditPhaseActive() {
        return "//div[@id='divProcess_510481BC-5774-4957-920F-C8323A843BF9'][not(contains(@style, 'display: none;'))]//div[@class='step active']";
    }

    public static String supportingDocuments() {
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//li[@id='tab_FAF9F96B-39CB-42EB-B50C-78DA19D365C0'][@class='active']";
    }

    public static String drugAlcohol() {
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//li[@id='tab_F45C2DD5-844A-40CA-BE41-7086A3E5ABCE'][@class='active']";
    }

    public static String drugAlcoholTab() {
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//li[@id='tab_F45C2DD5-844A-40CA-BE41-7086A3E5ABCE']";
    }

    public static String injuredPersonsFlow() {
        return "//div[@id='btnProcessFlow_form_510481BC-5774-4957-920F-C8323A843BF9']";
    }

    public static String ClickdrugAlcohol() {
        return "//div[text()='Drug and Alcohol Tests']";
    }

    public static String addInjuredPersonsButtonXPath() {
        return "//div[text()='Injured Persons']/..//div[@title='Add']";
    }

    public static String employeeTypeDropdownXPath() {
        return "//div[@id='control_C33A033C-530C-48D8-B6B3-41A96C4C614B']//li";
    }

    public static String employeeTypeSearchFieldXPath() {
        return "//*[@id='select3_0fe39394']/div[1]/input";
    }

    public static String employeeTypeDropdownElementXPath(String employee) {
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')]//a[text()='" + employee + "']";
    }

    public static String anySupervisorXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }

    public static String fullNameDropdownXPath() {
        return "//div[@id='control_02385642-D56E-4A37-8DB6-4DA2FFC79EED']//li";
    }

    public static String fullNameElementXPath(String name) {
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')]//a[text()='" + name + "']";
    }

    public static String injuryOrIlnessDropdownXPath() {
        return "//div[@id='control_6FED9E3D-6F13-4BD2-B693-36DB2E988E54']//li";
    }

    public static String injuryOrIlnessElementXPath(String element) {
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')]//a[text()='" + element + "']";
    }

    public static String drugAlcoholTestRequiredDropdownXPath() {
        return "//div[@id='control_D4C09E82-A49E-405D-B157-C05EAB13D89C']//li";
    }

    public static String dropdownElementXPath(String element) {
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='" + element + "']";
    }

    public static String treeElementXPath(String element) {
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='" + element + "']/..//i";
    }

    public static String jobProfileTextFieldXPath() {
        return "//div[@id='control_75EFF518-42FE-4C27-AFD0-7638706EDD66']//div//div//input";
    }

    public static String positionStartDatePickerXPath() {
        return "//div[@id='control_0C3F7E32-244A-4B7C-AB80-408EDDA4C2AC']//input";
    }

    public static String usualJobTasksXPath() {
        return "//div[@id='control_2DEC4C59-4121-4E55-A8A9-F26080CFDD28']//textarea";
    }

    public static String injuryOrIllnessClassificationXPath() {
        return "//div[@id='control_E059B2A9-5762-47DF-8C50-04ECC0F7CE14']//li";
    }

    public static String descriptionXPath() {
        return "//div[@id='control_1FD85823-9331-4C4D-8254-DEEE2722AD5A']//textarea";
    }

    public static String activityAtTheTimeDropdownXPath() {
        return "//div[@id='control_F9E96FD7-8789-4703-A315-FA4343063B1B']//li";
    }

    public static String bodyPartsDropdownXPath() {
        return "//div[@id='control_BB36171E-B07E-4A75-80C6-8932D4A6DD3A']//li";
    }

    public static String specificTreeItemXPath(String element) {//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='Injury']/..//i[@class='jstree-icon jstree-ocl']
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='" + element + "']/..//i[@class='jstree-icon jstree-ocl']";
    }

    public static String specificTreeItemCheckXPath(String element) {//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='Injury']/..//i[@class='jstree-icon jstree-ocl']
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='" + element + "']/..//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String bodyPartsSelectInjuredLocationsXPath(String element) {
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='" + element + "']/..//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String natureOfInjuryDropdownXPath() {
        return "//div[@id='control_B1770CDD-D005-43DC-8833-9CC55230D7AC']//li";
    }

    public static String mechanismDropdownXPath() {
        return "//div[@id='control_F68E8A27-7027-44DA-9804-6AD5524C3773']//li";
    }

    public static String natureOfInjuryLabelXPath() {
        return "//div[@id='control_2514FF32-01A9-423C-9F4F-FAAC50BC48ED']//div[text()='Nature of injury']";
    }

    public static String treatmentProvidedDescriptionXPath() {
        return "//div[@id='control_08613967-E400-4BC2-A972-EAC7FACD7F6F']//textarea";
    }

    public static String FollowUpRequiredCheckBoxXPath() {
        return "//div[@id='control_9D1737E8-037B-4722-9C13-DA1EB50A3022']//div[@class='c-chk']";
    }

    public static String followUpDetailsXPath() {
        return "//div[@id='control_0D9F346A-C77C-4E88-8F34-02E3225F3873']//textarea";
    }

    public static String AdditionalTreatmentRequiredCheckBoxXPath() {
        return "//div[@id='control_4E053A81-D6C1-46BE-B9A7-49862A87C970']//div[@class='c-chk']";
    }

    public static String TreatmentAwayFromWorkpaceCheckBoxXPath() {
        return "//div[@id='control_AC472479-B079-4B30-BC42-4522CB2D946B']//div[@class='c-chk']";
    }

    public static String ifTreatmentAwayFromWorkpaceCheckBoxXPath() {
        return "//div[@id='control_AC472479-B079-4B30-BC42-4522CB2D946B']//div[contains(@class, 'checked')]";
    }

    public static String TreatedInEmergencyRoomCheckBoxXPath() {
        return "//div[@id='control_A91CEA2B-C7C8-4A3B-8227-9653EF88826B']//div[@class='c-chk']";
    }

    public static String TreatedInpatientOverNightCheckBoxXPath() {
        return "//div[@id='control_27B303CB-E72B-4490-A100-F606240D3B4F']//div[@class='c-chk']";
    }

    public static String TreatmentFacililtyDetailsXPath() {
        return "//div[@id='control_8AFA35F7-2086-4E08-8FBD-6E0018C02286']//textarea";
    }

    public static String IsThisInjuryRecordableCheckBoxXPath() {
        return "//div[@id='control_70BC1E8B-CD32-44FE-B032-1C3464180DFD']//div[@class='c-chk']";
    }

    public static String IsItReportableCheckBoxXPath() {
        return "//div[@id='control_928AD9F0-9873-4242-8131-DEE2287A6AD6']//div[@class='c-chk']";
    }

    public static String ReportableToDropdownXPath() {
        return "//div[@id='control_A6CF2677-30BA-473B-8A86-F1756ED41EC8']//li";
    }

    public static String InjuryClaimCheckBoxXPath() {
        return "//div[@id='control_BE9780F4-5745-4FCF-BE94-6E798FA0C040']//div[@class='c-chk']";
    }

    public static String SaveAndContinueButtonXPath() {
        return "//div[@id='control_B08D329A-BA16-4EC9-AF30-22EEAD2E76AC']//div[text()='Save and continue ']";
    }

    public static String RecordSavedMessageXPath() {
        return "//div[@id='divMessage'][@class='ui floating icon message transition visible']//div[text()='Record saved']";
    }

    public static String ReturnToWorkManagementXPath() {
        return "//div[text()='Return to Work Management']";
    }

    public static String ReturnToWorkManagementAddButtonXPath() {
        return "//div[@id='control_0BBDE35F-D829-4440-B956-88FBBD23FE93']//div[text()='Add']";
    }

    public static String StatusDropdownXPath() {
        return "//div[@id='control_815C7F7D-D292-475B-B8CC-7BB62DB13B63']//li";
    }

    public static String OriginOfCaseDropdownXPath() {
        return "//div[@id='control_2C6A193C-1372-4651-AFBD-BF9328D375ED']//li";
    }

    public static String OriginOfCaseDeselectButtonOnDropdownXPath() {
        return "//div[@id='control_2C6A193C-1372-4651-AFBD-BF9328D375ED']//b[@original-title='De-select all']";
    }

    public static String OriginOfCaseValidateItemDropdownXPath(String text) {
        return "//div[@id='control_2C6A193C-1372-4651-AFBD-BF9328D375ED']//li[contains(text(),'" + text + "')]";
    }

    public static String ExternalCaseDropdownXPath() {
        return "//div[@id='control_4716CA6E-BF4F-4759-8C39-7017CB3963A3']//li";
    }

    public static String LinkToIncidentRecordDropdownXPath() {
        return "//div[@id='control_CB013909-8250-4437-9CD8-EB72696989D9']//li";
    }

    public static String DoctorsNoteDetailsHeadingXPath() {
        return "//div[@id='control_2786DFF9-407F-4592-85F3-717A78AE5C38']//span[text()]";
    }

    public static String MedicalPractionerDropdownXPath() {
        return "//div[@id='control_FEF66BA4-006C-4306-95FE-9C30F696EE00']//li";
    }

    public static String DoctorsNotesTextAreaXPath() {
        return "//div[@id='control_5A7D4F52-EB44-47FE-873B-91F487A402A8']//textarea";
    }

    public static String DateIssuedXPath() {
        return "//div[@id='control_0A4FBD9F-D7BE-40A4-B687-8F5AF269DC31']//input";
    }

    public static String FitnessForWorkDropdownXPath() {
        return "//div[@id='control_0F18182F-3C2B-4536-9ED8-3B762D2804BC']//li";
    }

    public static String DateFromXPath() {
        return "//div[@id='control_170034EC-65D8-43B5-95EC-757C9AA846E6']//input";
    }

    public static String DateToXPath() {
        return "//div[@id='control_6C837782-8216-4415-A790-550F66B561C1']//input";
    }

    public static String DateToHiddenXPath() {
        return "//div[@id='control_6C837782-8216-4415-A790-550F66B561C1'][contains(@style,'display: none;')]";
    }

    public static String UploadDocumentXPath() {
        return "//div[@id='control_DC169931-9139-4D33-AAA1-2C7E576EC010']//b[@original-title='Upload a document']";
    }

    public static String SuitableDuitiesHeadingXPath() {
        return "//div[@id='control_6B304FC8-1184-4DAF-9879-2761008826B4']//span[text()='Suitable Duties']";
    }

    public static String ReturnToWorkArrangementHeadingXPath() {
        return "//div[@id='control_5681BD07-84CC-4890-926F-A62443E914F2']//span[text()='Return to Work Arrangement']";
    }

    public static String ActionsAndSupportinhInformationHeadingXPath() {
        return "//div[@id='control_CCC6CB10-1851-4714-BD98-BA090D24C2A5']//span[text()='Actions & Supporting Information']";
    }

    public static String RehabilitationsProviderHeadingXPath() {
        return "//div[@id='control_A931C1C4-16AC-4922-BF1C-EFFBDC526190']//span[text()='Rehabilitation Provider']";
    }

    public static String AgreementsHeadingXPath() {
        return "//div[@id='control_2B017F25-7FB4-49F5-BB52-5C6071FEEA29']//span[text()='Agreements']";
    }

    public static String IsASuitableOfferOfEmploymentAttachedDropdownXPath() {
        return "//div[@id='control_B0570A51-0038-4201-8359-13C51128DA8F']//li";
    }

    public static String SecondUploadButton() {
        return "UploadSecond.PNG";
    }

    public static String DutieOrTasksToBeUndertakenTextAreaXPath() {
        return "//div[@id='control_4145C712-AFCB-42A1-A220-5955DC4BDA66']//textarea";
    }

    public static String WorkplaceSupportsTextAreaXPath() {
        return "//div[@id='control_3A6540C8-A597-4EFC-B291-88ABB428A9D1']//textarea";
    }

    public static String SpecificDutiesOrTasksTextAreaXPath() {
        return "//div[@id='control_523EB7C2-C6EB-433D-BCC1-4A1F1B09E95C']//textarea";
    }

    public static String ReturnToWorkArrangementTextAreaXPath() {
        return "//div[@id='control_6C94DBCC-DE8B-4B08-A170-3F311D9859FC']//textarea";
    }

    public static String ProviderNameTextFieldXPath() {
        return "//div[@id='control_E29D39AA-8DD7-4D2A-B6EE-8C89A6E6B041']//div//div//input";
    }

    public static String ContactNumberTextFieldXPath() {
        return "//div[@id='control_FE673810-F542-42C0-B971-DBB707A191E0']//div//div//input";
    }

    public static String EmailAddressTextFieldXPath() {
        return "//div[@id='control_4626A2B4-01D3-4470-8984-CEF0FB361D2A']//div//div//input";
    }

    public static String WorkersAgreementNameXPath() {
        return "//div[@id='control_AD63CBB3-6C69-4C31-8599-5131FC6C59EF']//div//div//input";
    }

    public static String WorkersAgreementDateOfAgreementXPath() {
        return "//div[@id='control_8BA22B58-2883-486E-821F-423BC126F91C']//input";
    }

    public static String SupervisorsAgreementNameXPath() {
        return "//div[@id='control_23E4FD0A-8181-46EA-AA45-303CF172545E']//div//div//input";
    }

    public static String SupervisorsAgreementDateOfAgreementXPath() {
        return "//div[@id='control_16C5568F-BB57-463D-B0D3-8CE35D55ACCA']//input";
    }

    public static String ReturnToWorkCoordinatorsAgreementNameXPath() {
        return "//div[@id='control_06F1838F-3A15-4B62-B5C6-AE33E8235B83']//div//div//input";
    }

    public static String ReturnToWorkCoordinatorsDateOfAgreementXPath() {
        return "//div[@id='control_EF7D2DC0-A90C-4E9F-958B-302A697B7B13']//input";
    }

    public static String TreatingMedicalPractionersAgreemenrNameXPath() {
        return "//div[@id='control_73C00A79-60B6-4567-81CE-9061838B9E5C']//div//div//input";
    }

    public static String TreatingMedicalPractionersAgreemenrDateOfAgreementXPath() {
        return "//div[@id='control_50504557-0CA4-4D81-89F6-FF089E004C36']//input";
    }

    public static String SaveUploadedReportButtonXPath() {
        return "//div[@id='control_54C06F49-468E-444A-940F-F4A0CC6C636D']//div[text()='Save Uploaded Report']";
    }

    public static String CloseFormButtonXPath() {
        return "//*[@id='form_F943DA67-2DF2-49E9-99CC-9B6FCA496AED']/div[1]/i[2]";
    }

    public static String CloseForm2ButtonXPath() {
        return "//*[@id='form_510481BC-5774-4957-920F-C8323A843BF9']/div[1]/i[2]";
    }

    public static String SaveFormButtonXPath() {
        return "//*[@id='form_F943DA67-2DF2-49E9-99CC-9B6FCA496AED']/div[1]/i[2]";
    }

    public static String SelectTestToBeConductedXPath(String value) {
        return "//div[@id='control_0E38919C-6050-4473-9DFC-C1BE2F438C06']//a[text()='" + value + "']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TestToBeConductedXPath() {
        return "//div[@class='nreqbox control pnl transition hidden']//div[@id='control_0E38919C-6050-4473-9DFC-C1BE2F438C06']";
    }

    public static String illnessDetailsTab() {
        return "//div[text()='Injury/Illness Detail']";
    }

    public static String ManagementWhoReferredThePersonXPath() {
        return "//div[@class='nreqbox control pnl transition hidden']//div[text()='Management who referred the person?']";
    }

    public static String ManagementWhoReferredThePersonLabelXPath() {
        return "//div[text()='Management who referred the person?']";
    }

    public static String ManagementWhoReferredThePersonDropdownXPath() {
        return "//div[@id='control_6E6CB335-293D-4FC4-8BED-AD70E9573E09']//li";
    }

    public static String ChronicIllnessPanelXPath() {
        return "//div[@label='Chronic illness management']";
    }

    public static String LinkToMedicalTestRecordFieldXPath() {
        return "//div[@id='control_59A61A40-9D8B-4920-AA48-7897FE7C2F17']/..//div[text()='Link to medical test record']";
    }

    public static String PersonBeingTestedXPath() {
        return "//div[@id='control_5D58831B-7752-493F-8EE5-A245C603B428']//li";
    }

    public static String LinkedIncidentXPath() {
        return "//div[@id='control_4342AE2C-BC1E-442B-AB70-4E0BF93F2515']//div[@class='c-chk']";
    }

    public static String LinkedIncidentDropdownXPath() {
        return "//div[@id='control_BE9FE046-7143-4819-8988-0E53681D3AF9']//li";
    }

    public static String GetIncidentRecordNumberXPath() {
        return "//div[@class='navbar']//div[contains(text(), '- Record #')]";
    }

    public static String LinkedIncidentTextFieldXPath() {
        return "//div[@id='select3_0e117628']//input";
    }

    public static String RecordNumberXPath() {
        return "//div[contains(text(),'Record #')]";
    }

    public static String OpenedRecordNumberXPath(String record) {
        return "//div[contains(text(),'Record #" + record + "')]";
    }

    public static String SaveAndContinueButton_DropDownXPath() {
        return "//div[@id='btnSave_form_CBE44FB8-CECF-4F77-8BC2-2A82882035E4']//div[@class='more options icon chevron down']";
    }

    public static String SaveAndCloseButtonXPath() {
        return "//div[@id='btnSaveClose_form_CBE44FB8-CECF-4F77-8BC2-2A82882035E4']//div[contains(text(),'Save and close')]";
    }
    
    public static String illnessonduty_dropown(){
        return "//div[@id='control_6FED9E3D-6F13-4BD2-B693-36DB2E988E54']";
    }
    
    public static String illnessonduty_select(String text){
        return "//a[text()='"+text+"']";
    }
}
