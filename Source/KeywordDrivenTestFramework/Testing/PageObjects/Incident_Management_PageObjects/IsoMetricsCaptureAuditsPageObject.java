/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

/**
 *
 * @author EMashishi
 */
public class IsoMetricsCaptureAuditsPageObject {

    public static String environmentalHealthSafetyXpath() {
        return "//div[@id='section_711036e0-fe8c-478f-a260-87beae7432fc']";
    }

    public static String auditsInspectionsXpath() {
        return "//label[contains(text(),'Audits & Inspections')]";
    }

    public static String auditsXpath() {
        return "//label[text()='Audits']";
    }

    public static String searchXpath() {
        return "//div[text()='Search']";
    }

    public static String addButtonXpath() {
        return "//div[text()='Add']";
    }

    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }

    public static String businessdownButtonXpath() {
        return "//div[@id='control_5B60ABCF-0B9E-4AF6-B604-8B8C45008B63']//b[@class='select3-down']";
    }

    //for now
    public static String miningXpath() {
        return "//li[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c']/i";
    }

    public static String southAfricaXpath() {
        return "//li[@id='bf1442de-757f-4536-96b7-a82489722126']/i";
    }

    public static String victoryMineXpath() {
        return "//li[@id='491eddcf-91b6-44db-b3fb-b3c89770f0c8']/i";
    }

    public static String oraProcessingXpath() {
        return "//a[@id='82afef96-e4bc-4504-bff7-6e017bcb3c1f_anchor']";
    }

    public static String proProjectTestXpath() {
        return "//a[@id='46d5865a-c305-4876-b75b-64406012fee6_anchor']";
    }

    public static String relatedStakeholderdropdownXpath() {
        return "//div[@id='control_74BC552C-C07A-486B-AC3F-1174C339C5BE']//b[@class='select3-down']";
    }

    public static String stakeHolderAmaDwalaXpath() {
        return "//a[@id='03f4e023-e609-4a57-a0f5-75c3ce6947e8_anchor']";
    }

    public static String projectCheckBoxXpath() {
        return "//input[@id='Linktoproject']//..//..//div";
    }

    public static String projectdropdownXpath() {
        return "//div[@id='control_5F3EB691-4CF3-4CA2-891E-734354744DD4']//b[@class='select3-down']";
    }

    public static String riskdisciplinedDownXpath() {
        return "//div[@id='control_7333CA9A-96BE-486A-B8F4-DC07A0D58268']//b[@class='select3-down']";
    }

    public static String complianceCheckBoxXpath() {
        return "//a[@id='b38c4a28-05f9-423d-a53e-3d2940d29f6f_anchor']//..//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String SafetyCheckBoxXpath() {
        return "//a[@id='f86e0bdd-b06d-4122-a92a-2b8dcb4ba17d_anchor']//..//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String riskdisciplinedUpXpath() {
        return "//b[@class='select3-down drop_click']";
    }

    public static String auditTypeDownXpath() {
        return "//div[@id='control_762578C8-DE68-4025-B029-4FC1FAB832B5']//b[@class='select3-down']";
    }

    public static String auditTypeInternalXpath() {
        return "//a[@id='daab6ab6-eef3-4edd-875b-560db3920e7d_anchor']";
    }

    public static String auditProtocolDownXpath() {
        return "//div[@id='control_464907F1-B84F-4A4F-8228-7BDAF119EE53']//b[@class='select3-down']";
    }

    public static String Iso9001Xpath() {
        return "//li[@id='29735a53-171d-4c7c-8e29-4e01b54fb234']/i";
    }

    public static String humanResXpath() {
        return "//a[@id='050cc5b3-1674-489a-b4c2-f99198ea30e3_anchor']";
    }

    public static String businessProcessAFXpath() {
        return "//li[@id='f77c1187-b656-4f98-a48b-18c259d85161']/i";
    }

    public static String businessProcessAMpath() {
        return "//a[@id='8f6ff326-9f06-4540-a3dd-b344c26d534b_anchor']/i[@class='jstree-icon jstree-checkbox']";
    }

    public static String strDateXpath() {
        return "//div[@id='control_40752CA7-D6F1-429B-BF6E-ADD0A20A8FC0']//input[@class='k-input']";
    }

    public static String strEndDate() {
        return "//div[@id='control_4ACB9871-78C2-4168-B78A-CCF6CD3FB877']//input[@class='k-input']";
    }

    //Audit Manager
    public static String auditManagerDownXpath() {
        return "//div[@id='control_D718214C-2A59-46A6-A6E8-7F5FDFBBF85F']//b[@class='select3-down']";
    }

    public static String auditManagerAdminXpath() {
        return "//a[text()='1 Administrator'][@class='jstree-anchor']";//div[@style='left: 1221px; width: 712px; top: auto; bottom: 254px;display: block !important;']//li[1]//a";
    }

    public static String managerXpath() {
        return "//a[text()='2 Manager']";
    }

    //Auditee
    public static String auditDownXpath() {
        return "//div[@id='control_22098F99-2305-40B1-85C3-860634B5B5F1']//b[@class='select3-down']";
    }

    public static String auditManagerXpath() {
        return "//div[@style='left: 1221px; width: 712px; top: auto; bottom: 222px;display: block !important;']//li[2]//a";
    }

//div[@id='select3_cdefab35']//li[@id='b8ad5f90-d582-46c4-b186-d99649824acd']
    //Person conducting the audit
    public static String personCondAuditDownXpath() {
        return "//div[@name='ddl5']//li";
    }

    public static String SikuliImagePath() {
        return "\\SikuliImages\\";
    }

    public static String personCondAuditDownPic() {
        return "Admin.PNG";
    }

    public static String personCondAuditManagerXpath() {
        return "//div[@style='left: 1221px; width: 712px; top: auto; bottom: 184px;display: block !important;']//li[1]//a";
    }

    //Audit Reference
    public static String auditReferenceText() {

        return "//div[@id='control_2DD40409-F54E-422B-99FF-35E15216E9B0']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    //Introduction
    public static String ntroductionText() {

        return "//div[@id='control_5550C644-1C46-4904-A7F7-01648A0B3674']//textarea";
    }

    //Audit Objective
    public static String auditObjectiveText() {
        return "//div[@id='control_C559A407-5194-4245-83E3-7EDDB7598A8E']//textarea";
    }

    //Audit scope
    public static String auditScopeText() {
        return "//div[@id='control_04975551-BEE4-4278-9E47-77224B6CA1AA']//textarea";
    }

    //Application Section to be audits 
    public static String applicableSectionAuditsXpath() {
        return "//li[@id='fbc730db-2fd9-4535-a15e-12fbf5a14faf']/i";
    }

    public static String performanceReXpath() {
        return "//li[@id='ad71f5a0-2894-4a3e-9a8c-9bca14cb41d2']/i";
    }

    public static String verifyOMAPXpath() {
        return "//a[@id='73e34cd8-9ef5-47a2-a729-b49de2fb7943_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String saveXpath() {
        return "//div[@id='btnSave_form_C9BF4B41-F36D-48FC-8F3F-FE6BC39DEE07']//div[text()='Save']";
    }//span[@title='Pin process flow']

      //Getting text
    public static String unitTextXpath() {
        return "//div[@id='control_5B60ABCF-0B9E-4AF6-B604-8B8C45008B63']//li";
    }

    public static String projectTextXpath() {
        return "//div[@id='control_5F3EB691-4CF3-4CA2-891E-734354744DD4']//li";
    }

    public static String stakeHoldersTextXpath() {
        return "//div[@id='control_74BC552C-C07A-486B-AC3F-1174C339C5BE']//li";
    }

    public static String riskTextXpath() {
        return "//div[@id='control_7333CA9A-96BE-486A-B8F4-DC07A0D58268']//li";
    }

    public static String section() {
        return "//li[@id='73e34cd8-9ef5-47a2-a729-b49de2fb7943']";
    }

    public static String auditTypeTextXpath() {
        return "//div[@id='control_762578C8-DE68-4025-B029-4FC1FAB832B5']//li";
    }

    public static String auditProtocolTextXpath() {
        return "//div[@id='control_762578C8-DE68-4025-B029-4FC1FAB832B5']//li";
    }

    public static String businessProTextXpath() {
        return "//div[@id='control_EEA456A2-6A09-4F8B-85AB-C2F3E0581B03']//li";
    }

    public static String selectFromDropdown(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }

}
