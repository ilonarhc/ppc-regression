/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author MJivan
 */
public class IsometricsRiskAssessmentScopePageObjects {

    public static String environmentalHealthSafetyXpath() {
        return "//div[@id='section_711036e0-fe8c-478f-a260-87beae7432fc']";
    }

    public static String riskManagementButtonXpath() {
        return "//div[@id='section_718fb7f6-442a-42cb-a794-79f586dc1886']";
    }

    public static String integratedRiskRegisterButtonXpath() {
        return "//div[@id='section_cae3a0a4-7aa2-4769-91ac-73e4b5a9c5cf']";
    }

    public static String riskManagementAddButtom() {
        return "//div[text()='Add']";
    }

    public static String rmDateOfAssXpath() {
        return "//div[@id='control_BA27D154-028F-44EF-BDB9-E51252672697']//input";
    }
    
    public static String rmBusinessUnitXpath(String name) {
        return "//a[text()='"+name+"']//../i[@class='jstree-icon jstree-ocl']";
    }
    
    public static  String businessUnit(String name){
        return "//a[text()='"+name+"']//../a";
    }
    
    public static String riskdiscipline(String name){
        return "//a[text()='"+name+"']//../i[@class='jstree-icon jstree-checkbox']";
    }
    
     public static String riskdiscipline1(String name){
        return "//a[text()='"+name+ " ']//../i[@class='jstree-icon jstree-checkbox']";
    }
    
    
 
     public static String riskdisciplinedDownXpath() {
        return "//div[@id='control_D0E8F3FA-BD27-4584-B51E-6B49F16D0480']";
    }
     
     public static String businessUnitdropDownXpath(){
         return "//div[@id='control_A1B23A4E-893B-46AB-9C47-AF967BE14AB7']";
     }
     
     public static String nothing(){
         return "//div[text()='Business Unit']";
     }
     
     public static String riskAssessmentRef(){
         return "//div[@id='control_1C1EE7E7-CA6C-44F0-9259-F631A42BF48E']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
     }
     
     public static String scopeTextareaXpath(){
         return "//div[@id='control_ED3FCD7C-F73F-4CE8-9030-08CE80DDA7CD']//textarea";
     }
     
     public static String scopeCategoryDropDownXpath(){
         return "//div[@id='control_7980E64B-9FFC-4251-8A14-227AEEFE7309']";
     }
     
     public static String scopeCategory(String name){
         return "//a[text()='"+name+"']";
     }
     
     public static String businessProcessDropDownXpath(){
         return "//div[@id='control_7980E64B-9FFC-4251-8A14-227AEEFE7309']";
     }
     
     public static String responsiblePersonDropdownXpath(){
         return "//div[@id='control_25EB2D38-F2AA-4689-94E5-36BABF04BBF9']";
     }
     
      public static String CreateRiskAssessmenDropdownXpath(){
         return "//div[@id='control_D4AC91B6-E1EF-447E-AE02-CFBCF78907A1']";
     }
      
      public static String risksource(String name){
          return "//a[text()='"+name+"']//../i[@class='jstree-icon jstree-ocl']";
      }
      
      public static String saveButtonXpath(){
          return "//div[@id='btnSave_form_3AB45051-222E-416C-AAD3-86B2EDA98BED']";
      }
      
      
      public static String riskSourceXpath(){
          return "//div[@id='grid']/div[3]/table//tr";
      }
      
      public static String tableRiskSource(int num){
          return "//div[@id='grid']/div[3]/table//tr["+num+"]//td[4]/div";
      }

    

    
   

}
