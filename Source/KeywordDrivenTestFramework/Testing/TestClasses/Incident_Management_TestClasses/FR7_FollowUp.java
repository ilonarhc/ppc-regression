/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Follow up required",
        createNewBrowserInstance = false
)

public class FR7_FollowUp extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR7_FollowUp() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!FR7_FollowUp()) {
            return narrator.testFailed("Failed fill Injured Persons - Follow up required - " + error);
        }

        return narrator.finalizeTest("Completed Injured Persons - Follow up required ");
    }

    public boolean FR7_FollowUp() {

        if (getData("Follow up required").equalsIgnoreCase("True")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.illnessDetailsTab())) {
                error = "Failed to click the injury or illness tab ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.FollowUpRequiredCheckBoxXPath())) {
                error = "Failed to click on the Follow Up Required checkbox ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.followUpDetailsXPath(), 2)) {
                error = "Failed to display Follow Up Details text area  ";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.followUpDetailsXPath(), getData("Follow Up Details"))) {
                error = "Failed to fill in the Follow Up Details text area: " + getData("Follow Up Details");
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully displayed Follow up details");

        } else if (getData("Follow up required").equalsIgnoreCase("false")) {

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.FollowUpRequiredCheckBoxXPath(), 2)) {
                error = "Follow up details is displayed. ";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(InjuredPersonsPageObjects.FollowUpRequiredCheckBoxXPath())) {
                error = "Failed to scroll on the Follow Up Required checkbox ";
                return false;
            }
            narrator.stepPassedWithScreenShot(" Drug and alcohol test No tabs are triggered ");
        }

        return true;
    }

}
