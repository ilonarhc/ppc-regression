/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.EnvironmentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Why Analysis",
        createNewBrowserInstance = false
)

public class FR20_WhyAnalysis extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public FR20_WhyAnalysis() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!EnvironmentSpill()) {
            return narrator.testFailed("Failed to fill out the Environment Spill details - " + error);
        }

        return narrator.finalizeTest("Successfully Record is saved and automatically moves to the Edit phase");
    }

    public boolean EnvironmentSpill() {

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.threeIncidentsInvesgationXpath())) {
            error = "Failed to wait for the investigation details tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.threeIncidentsInvesgationXpath())) {
            error = "Failed to click on the investigation details tab ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.InvestigationDetailTabXPath())) {
            error = "Failed to wait for the investigation details sub tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationDetailTabXPath())) {
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.ProcessActivityDropdownXPath())) {
            error = "Failed to click on the Process/Activity dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(getData("Process Activity 1")))) {
            error = "Failed to click on the Process/Activity option checkbox - " + getData("Process Activity 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Process Activity 2")))) {
            error = "Failed to click on the Process/Activity option checkbox - " + getData("Process Activity 2");
            return false;
        }

//        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.specificTreeItemXPath(getData("Process Activity 1")))) {
//            error = "Failed to click on the Process/Activity option checkbox - " + getData("Process Activity 1");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Process Activity 2")))) {
//            error = "Failed to click on the Process/Activity option checkbox - " + getData("Process Activity 2");
//            return false;
//        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationDetailTabXPath())) {
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.RiskSourceDropdownXPath())) {
            error = "Failed to click on the Risk Source dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.moveToElementByXpath(Isometrics_PageObjects.MechanicalSelectionXPath())) {
            error = "Failed to click on the Risk Source option checkbox - " + "Mechanical";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.MechanicalSelectionXPath())) {
            error = "Failed to click on the Risk Source option checkbox - " + "Mechanical";
            return false;
        }
        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk Source 2")))) {
            error = "Failed to click on the Risk Source option checkbox - " + "Mechanical";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk Source 2")))) {
            error = "Failed to click on the Risk Source option checkbox - " + getData("Risk Source 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.RiskDropdownXPath1())) {
            error = "Failed to click Risk Source close dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.RiskDropdownXPath())) {
            error = "Failed to click on the Risk dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk")))) {
            error = "Failed to click on the Risk option checkbox - " + getData("Risk");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationTypeDropdownXPath1())) {
            error = "Failed to close on Risk dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.InvestigationTypeDropdownXPath())) {
            error = "Failed to click on the Investigation Type dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Investigation Type")))) {
            error = "Failed to click on the Investigation Type option - " + getData("Investigation Type");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.whyAnalysisPanel())) {
            error = "Failed to click on the why analysis panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.WhyAnalysisAddButton())) {
            error = "Failed to click on the why Analusis add button ";
            return false;
        }


        if (!SeleniumDriverInstance.waitForElementByXpath(Isometrics_PageObjects.whyAnalysisOrder(), 3)) {
            if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.WhyAnalysisAddButton())) {
                error = "Failed to click on the why Analusis add button ";
                return false;
            }
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.whyAnalysisOrder(), getData("Order Number"))) {
            error = "Failed to click on the why Analusis Oder Number";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.whyAnalysisText(), getData("why"))) {
            error = "Failed to click on the why Analusis - Why";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Isometrics_PageObjects.whyAnalysisAnswerText(), getData("Answer"))) {
            error = "Failed to click on the why Analusis - Answer";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Isometrics_PageObjects.saveWhyAnalysisXpath())) {
            error = "Failed to click on the why Analusis - save";
            return false;
        }
        
          if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }


        narrator.stepPassedWithScreenShot("Successfulled saved why Analysis");

//        String submit = getData("Submit");
//        String[] records = SeleniumDriverInstance.retrieveTextByXpath(EnvironmentPageObjects.envirSpillRecordNumXpath()).split("#");
//        String recordNum = records[1];
//
//        if (submit.equalsIgnoreCase("True")) {
//            if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.SubmitButtonXPath())) {
//                error = "Failed to click the Submit button";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(EnvironmentPageObjects.validateEnvirSpillgridSavedRecord(recordNum), 14)) {
//                error = "Failed to save record in Incident Management edit page " + recordNum;
//                return false;
//            }
//
//            narrator.stepPassedWithScreenShot("Successfully save record #" + recordNum + " Environmental Spill ");
//
//        }

        return true;
    }

}
