/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Save and Continue to step 2 close",
        createNewBrowserInstance = false
)
public class IncidentCaptureDetailsAndsaveStep1 extends BaseClass {

    String error = "";

    public IncidentCaptureDetailsAndsaveStep1() {

    }

    public TestResult executeTest() {

        if (!captureDetailsAndsaveStep2()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed Submited record");
    }

    public boolean captureDetailsAndsaveStep2() {

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.submitXpath())) {
            error = "Failed to click to submit";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveIncidents(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

//        if (!SeleniumDriverInstance.scrollToElement(IsoMetricsIncidentMainScenarioPageObject.addDetailsXpath())) {
//            error = "Failed to Save and continue to Step 2";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentMainScenarioPageObject.addDetailsXpath())) {
//            error = "Failed to click to Save and continue to Step 2";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.personInvolvedXpath(), 1)) {
//            error = "Failed to display Persons Involved";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.witnessStatementsXpath(), 1)) {
//            error = "Failed to display witness Statements";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.euipmentInvolvedTabXpath(), 1)) {
//            error = "Failed to display Equipment Involved";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.Safety(), 1)) {
//            error = "Failed to display Safety tab";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.EnvironmentXpath(), 1)) {
//            error = "Failed to display Environment tab";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.QualitytXpath(), 1)) {
//            error = "Failed to display Quality tab";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.railwaySafetyXpath(), 1)) {
//            error = "Failed to display Railway Safety tab";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.socialSusXpath(), 1)) {
//            error = "Failed to display Social Sustainability tab";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.occHygieneXpath(), 1)) {
//            error = "Failed to display Occupational Hygiene tab";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.complianceXpath(), 1)) {
//            error = "Failed to display Compliance tab";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementsbyXpath(IncidentPageObjects.clickClose(), 0)) {
//            error = "Failed to click incident management close button";
//            return false;
//        }
//        
        SeleniumDriverInstance.pause(1500);
//

        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.incidentRefresh(),6)) {
            error = "Failed to wait to incident management refresh button";
            return false;
        }


        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.incidentRefresh())) {
            error = "Failed to click to incident management refresh button";
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.tablexpath(),40)) {
            error = "Failed to wait to incident management ";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.recordXpath(getRecordId()),20)) {
//            error = "Failed to click to incident management record id : "+getRecordId();
//            return false;
//        }
        narrator.stepPassed("Successfully saved record ");

        return true;
    }
}
