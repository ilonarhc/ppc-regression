/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsInprogressAuditPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsScheduleAuditPageObject;

/**
 *
 * @author MJivan
 */
@KeywordAnnotation(
        Keyword = "InProgress",
        createNewBrowserInstance = false
)
public class IsoMetricsInprogress extends BaseClass {

    String error = "";

    public IsoMetricsInprogress() {

    }

    public TestResult executeTest() {

        if (!findingDetails()) {
            return narrator.testFailed("Failed to Finding Details - " + error);
        }

        if (!actionDetails()) {
            return narrator.testFailed("Failed to Action Detail - " + error);
        }

        if (!actionFeedback()) {
            return narrator.testFailed("Failed to Action feedback - " + error);
        }

        return narrator.finalizeTest("Successfully Inprogress Audit");
    }

    public boolean findingDetails() {

        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveWait2(), 2000)) {
                error = "Website too long to load ";
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.findingButtonXpath())) {
            error = "Failed to click add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.strtAuditCheckBoxXpath())) {
            error = "Failed to click findings checkbox";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.addButtomXpath())) {
            error = "Failed to click findings add button";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveWait2(), 20)) {
                error = "Website too long to load ";
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.auditElementSDown())) {
            error = "Failed to click Audit Element";
            return false;
        }

        SeleniumDriverInstance.pause(3500);
        
        if (!SeleniumDriverInstance.clickElementsbyXpath(IsoMetricsInprogressAuditPageObject.ISOXpath(), 1)) {
            error = "Failed to click OS";
            return false;
        }

        SeleniumDriverInstance.pause(1500);
        
        if (!SeleniumDriverInstance.clickElementsbyXpath(IsoMetricsInprogressAuditPageObject.performanceEvXpath(), 1)) {
            error = "Failed to enter type of search";
            return false;
        }
        
        SeleniumDriverInstance.pause(1500);

        if (!SeleniumDriverInstance.clickElementsbyXpath(IsoMetricsInprogressAuditPageObject.verifyOrXpath(), 1)) {
            error = "Failed to enter type of search";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsInprogressAuditPageObject.decriptionXpath(), getData("Finding description"))) {
            error = "Failed to enter finding description";
            return false;
        }
        
         if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.findingOwnerDownXpath())) {
            error = "Failed to click  Finding owner";
            return false;
        }

        
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.selectFromDropdow1n(getData("Finding owner")))) {
            error = "Failed to click  Finding owner";
            return false;
        }

       

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.hazardDownpath())) {
            error = "Failed to click  Risk source dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.classChemicalXpath())) {
            error = "Failed to click  Risk source dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.nothing())) {
            error = "Failed to click  nothing";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.findingClassDownpath())) {
            error = "Failed to click  Finding classification dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.findingClassRecoXpath())) {
            error = "Failed to click  Finding classification";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.rootCauseDownXpath())) {
            error = "Failed to click  root cause";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.workplace())) {
            error = "Failed to click workplace factor";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.inadequateTrainging())) {
            error = "Failed to click knowledge transfer ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.noTraingingPro())) {
            error = "Failed to click no training provided ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.newWork())) {
            error = "Failed to click new work methods intro ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.nothing())) {
            error = "Failed to click nothing ";
            return false;
        }

        narrator.stepPassed("Audit Elements : " + SeleniumDriverInstance.retrieveTextByXpath(IsoMetricsInprogressAuditPageObject.auditElememtText()));
        narrator.stepPassed("Finding Owner : " + SeleniumDriverInstance.retrieveTextByXpath(IsoMetricsInprogressAuditPageObject.findingOwnerTextXpath()));
        narrator.stepPassed("Risk source / Hazard : " + SeleniumDriverInstance.retrieveTextByXpath(IsoMetricsInprogressAuditPageObject.hazardTextXpath()));
        narrator.stepPassed("Finding classification : " + SeleniumDriverInstance.retrieveTextByXpath(IsoMetricsInprogressAuditPageObject.findingClassificationTextXpath()));
        narrator.stepPassed("Root cause : " + SeleniumDriverInstance.retrieveTextByXpath(IsoMetricsInprogressAuditPageObject.rootCauseTextXpath()));
        narrator.stepPassedWithScreenShot("Successfully entered Finding Details");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.saveFindingDetialsXpath())) {
            error = "Failed to click save finding Details ";
            return false;
        }

        return true;
    }

    public boolean actionDetails() {
        
        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 4)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveLoading(), 200)) {
                error = "Website too long to load ";
                return false;
            }
        }if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 4)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveLoading(), 20)) {
                error = "Website too long to load ";
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.addButtonAuditXpath())) {
            error = "Failed to click add Audit finding Action ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsInprogressAuditPageObject.actionDescriptionXpath(), getData("Action description"))) {
            error = "Failed to click add Audit finding Action ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.departmentResponsibleDownXpath())) {
            error = "Failed to click department responsible dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.selectFromDropdown(getData("department responsible")))) {
            error = "Failed to click department responsible ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.responsiblePersonDownXpath())) {
            error = "Failed to click responsible person down";
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        
        if (!SeleniumDriverInstance.clickElementsbyXpath(IsoMetricsInprogressAuditPageObject.managerXpath(), 5)) {
            error = "Failed to click responsible person";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsInprogressAuditPageObject.actionDueDate(), endDate)) {
            error = "Failed to click action due date ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.replicateCheckboxXpath())) {
            error = "Failed to click replicate this action to multiple users checkbox ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.UserAdmim())) {
            error = "Failed to click replicate this action to multiple users ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.saveActionDetails())) {
            error = "Failed to click save ";
            return false;
        }

        return true;
    }

    public boolean actionFeedback() {

        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 4)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveWait3(), 200)) {
                error = "Website too long to load ";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.actionFeedBackXpath())) {
            error = "Failed to click Action FeedBack ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.addButtonActionFeedBackXpath())) {
            error = "Failed to click add Action FeedBack button ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsInprogressAuditPageObject.actionFeedbackTextArea(), getData("Action feedback"))) {
            error = "Failed to enter add Action FeedBack ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.actionCompletedownXpath())) {
            error = "Failed to click add Action complete dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.actionCompleteXpath(getData("Action Complete")))) {
            error = "Failed to click Action complete " + getData("Action Complete");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementsbyXpath(IsoMetricsInprogressAuditPageObject.actionFeedbackXpath(getData("Send action feedback to:")), 1)) {
            error = "Failed to click Send action feedback to: " + getData("Send action feedback to:");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.saveActionFeedbackXpath())) {
            error = "Failed to click Save button ";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered Audit Feedback");

        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveWait4(), 200)) {
                error = "Website too long to load ";
                 return false;
            }
        }

       

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsInprogressAuditPageObject.closeActionFeedBack())) {
            error = "Failed to click close button ";
            return false;
        }
        
     
        return true;
    }

}
