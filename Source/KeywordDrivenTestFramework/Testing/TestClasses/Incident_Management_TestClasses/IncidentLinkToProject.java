/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import org.openqa.selenium.By;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Link to a project",
        createNewBrowserInstance = false
)
public class IncidentLinkToProject extends BaseClass {

    String error = "";

    public IncidentLinkToProject() {

    }

    public TestResult executeTest() {

        if (!linkProject()) {
            return narrator.testFailed("Failed to logging An Incident due - " + error);
        }

        return narrator.finalizeTest("Completed Link to a project");
    }

    public boolean linkProject() {

        if (getData("Link to Project").equalsIgnoreCase("True")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.ProjectLink())) {
                error = "Failed to click Project Link";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Project())) {
                error = "Failed to click project dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.anyClickOptionXpath(getData("Project")))) {
                error = "Failed to click project " + getData("Project");
                return false;
            }

            narrator.stepPassed("Successfully selected and checked Link to a project :" + SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.projectText())).getText());
        } else {

            if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.linkProjectChecked(),1)) {
                if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.ProjectLink())) {
                    error = "Failed to Unchecke link project checkbox";
                    return false;
                }
            }

            if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.Project(), 1)) {
                error = "Failed to disable project ";
                return false;
            }

            

            narrator.stepPassed("Do not tick the Project link checkbox.");

        }

        return true;
    }
}
