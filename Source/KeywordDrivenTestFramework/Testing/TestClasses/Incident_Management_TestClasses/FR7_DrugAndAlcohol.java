/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Drug and alcohol test required",
        createNewBrowserInstance = false
)

public class FR7_DrugAndAlcohol extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR7_DrugAndAlcohol() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!drugAndAlcohol()) {
            return narrator.testFailed("Failed fill Injured Persons - Drug and alcohol test required - " + error);
        }

        return narrator.finalizeTest("Completed Injured Persons - Drug and alcohol test required ");
    }

    public boolean drugAndAlcohol() {

        if (getData("Drug and alcohol test required").equalsIgnoreCase("Yes")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.drugAlcoholTestRequiredDropdownXPath())) {
                error = "Failed to click the drugs and alcohol test dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Drug and alcohol test required")))) {
                error = "Failed to click the drugs and alcohol test element: " + getData("Drug and alcohol test required");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.ClickdrugAlcohol(), 2)) {
                error = "Failed to display Drug and Alcohol Tests tab";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.ClickdrugAlcohol())) {
                error = "Failed to click Drug and Alcohol Tests tab";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully displayed Drug and alcohol test");

        } else if (getData("Drug and alcohol test required").equalsIgnoreCase("No")) {
            
           

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.drugAlcoholTestRequiredDropdownXPath())) {
                error = "Failed to click the drugs and alcohol test dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Drug and alcohol test required")))) {
                error = "Failed to click the drugs and alcohol test element: " + getData("Drug and alcohol test required");
                return false;
            }

            narrator.stepPassedWithScreenShot(" Drug and alcohol test No tabs are triggered ");
        }

        return true;
    }

}
