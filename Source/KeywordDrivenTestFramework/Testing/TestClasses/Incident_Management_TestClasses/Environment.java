/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.EnvironmentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Environment",
        createNewBrowserInstance = false
)

public class Environment extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public Environment()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest()
    {
        if (!EnvironmentSpill())
        {
            return narrator.testFailed("Failed to fill out the Environment Spill details - " + error);
        }
        if (!Monitoring())
        {
            return narrator.testFailed("Failed to fill out the Monitoring details - " + error);
        }

        return narrator.finalizeTest("Successfully");
    }

    public boolean EnvironmentSpill()
    {
        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.EnvironmentTabXPath()))
        {
            error = "Failed to click on the Environment tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.MonitoringRequiredCheckboxXPath()))
        {
            error = "Failed to check Monitoring Required checkbox";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.UncontrolledReleaseOrSpillXPath()))
        {
            error = "Failed to click on the Uncontrolled Release/Spill heading";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.UncontrolledReleaseOrSpillAddButtonXPath()))
        {
            error = "Failed to click on the Uncontrolled Release/Spill add button ";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait2(), 10))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 500))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 4))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 500))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.ProductDropdownXPath()))
        {
            error = "Failed to click on the Product dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Product"))))
        {
            error = "Failed to select the following item: " + getData("Product");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(EnvironmentPageObjects.QualityReleasedTextFieldXPath()))
        {
            error = "Failed to double click on the Quality Released text field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EnvironmentPageObjects.QualityReleasedTextFieldXPath(), getData("Quality released")))
        {
            error = "Failed to enter text into the Quality Released text field";
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(EnvironmentPageObjects.QualityContainedTextFieldXPath()))
        {
            error = "Failed to double click on the Quality Contained text field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EnvironmentPageObjects.QualityContainedTextFieldXPath(), getData("Quality contained")))
        {
            error = "Failed to enter text into the Quality Contained text field";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.UnitOFMeasureDropdownXPath()))
        {
            error = "Failed to click on the Unit OF Measure Dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Unit Of Measure"))))
        {
            error = "Failed to select the following item: " + getData("Unit Of Measure");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EnvironmentPageObjects.DetailedDescriptionTextAreaXPath(), getData("Detailed description")))
        {
            error = "Failed to enter text into the Detailed Description Text Area";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EnvironmentPageObjects.CleanUpCostTextFieldXPath(), getData("Clean Up Cost")))
        {
            error = "Failed to enter text into the Clean Up Cost Text Field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully filled in the Environment Spill form");
        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.SubmitButtonXPath()))
        {
            error = "Failed to click the Submit button";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(EnvironmentPageObjects.ValidateRecordWasCreatedXPath()))
        {
            error = "Failed to validate that the record was created";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.UncontrolledReleaseOrSpillXPath()))
        {
            error = "Failed to click on the Uncontrolled Release/Spill heading";
            return false;
        }
        return true;
    }

    
    public boolean Monitoring()
    {
        if (!SeleniumDriverInstance.moveToElementByXpath(EnvironmentPageObjects.MonitoringHeadingXPath(), 100))
        {
            error = "Failed to click on the Monitoring heading";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.SelectAllButtonMonitoringAreasXPath()))
        {
            error = "Failed to click on the Selet All button for the Monitoring Areas selection";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.WaterTabXPath()))
        {
            error = "Failed to click on the Water tab";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EnvironmentPageObjects.ValidateWaterValuesXPath()))
        {
            error = "Failed to wait for the Water details to load";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.AirTabXPath()))
        {
            error = "Failed to click on the Air tab";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EnvironmentPageObjects.ValidateAirValuesXPath()))
        {
            error = "Failed to wait for the Air details to load";
            return false;
        }
        
        return true;
    }
    
    public boolean QualityConcessions()
    {
        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.QualitySubTab()))
        {
            error = "Failed to click on the Quality tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.QualityConcessionsXPath()))
        {
            error = "Failed to click on the Quality Concessions tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.ConcessionsAddButtonXPath()))
        {
            error = "Failed to click on the Concessions add button";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait2(), 4))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 500))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 4))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 500))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(EnvironmentPageObjects.CleanUpCostTextFieldXPath(), getData("Clean Up Cost")))
        {
            error = "Failed to enter text into the Clean Up Cost Text Field";
            return false;
        }
//         if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.()))
//        {
//            error = "Failed to click the Reportable to Dropdown";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Regulatory authority"))))
//        {
//            error = "Failed to select the Reportable to item: " + testData.getData("Regulatory authority");
//            return false;
//        }

        
        return true;
    }
}
