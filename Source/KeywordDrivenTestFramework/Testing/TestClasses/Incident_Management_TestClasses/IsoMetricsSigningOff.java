/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricSignOffPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsCaptureAuditsPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsInprogressAuditPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsScheduleAuditPageObject;

/**
 *
 * @author MJivan
 */
@KeywordAnnotation(
        Keyword = "SighOff",
        createNewBrowserInstance = false
)
public class IsoMetricsSigningOff extends BaseClass {

    String error = "";

    public IsoMetricsSigningOff() {

    }

    public TestResult executeTest() {

        if (!SignOffOne()) {
            return narrator.testFailed("Failed to Sign off one Details - " + error);
        }

        if (!SignOffTwo()) {
            return narrator.testFailed("Failed to Send for sign off Detail - " + error);
        }

        if (!auditsignOff()) {
            return narrator.testFailed("Failed to audit sign Off- " + error);
        }

        return narrator.finalizeTest("Successfully Inprogress Audit");
    }

    public boolean SignOffOne() {

        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveWait2(), 20)) {
                error = "Website too long to load ";
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.sighOffbuttonXpath())) {
            error = "Failed to click Sigh off button of Audit finding Action";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.sighOffAddbuttonXpath())) {
            error = "Failed to click Sigh off Add button of Audit finding Action";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.sighOffActionDownXpath())) {
            error = "Failed to click Sigh off action dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.sighOffActionXpath(getData("Sign off action?")))) {
            error = "Failed to selected Sigh off action " + getData("Sign off action?");
            return false;
        }
        narrator.stepPassed("Successfully seletec Sign off Action?: " + getData("Sign off action?"));

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricSignOffPageObject.sighOffCommentsXpath(), getData("Comments"))) {
            error = "Failed to enter action Sigh off comments " + getData("Comments");
            return false;
        }

        narrator.stepPassed("Successfully entered Action Sign off comments: " + getData("Comments"));

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.sighOffSendFeedBackDownXpath(getData("Send feedback to additional users")))) {
            error = "Failed to click send feedback to additional users";
            return false;
        }

        narrator.stepPassed("Successfully selected send feedback to Additional users " + getData("Send feedback to additional users"));

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.sighOffSaveButton())) {
            error = "Failed to click save action sigh off button ";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered Finding Details");

          if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveWait2(), 20)) {
                error = "Website too long to load ";
            }
        }


        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.sighOffCloseButton())) {
            error = "Failed to click close Action sigh off button";
            return false;
        }

        SeleniumDriverInstance.pause(2000);
         if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveWait2(), 20)) {
                error = "Website too long to load ";
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.auditFindingsXpath())) {
            error = "Failed to click close Audit finding button";
            return false;
        }

        return true;
    }

    public boolean SignOffTwo() {
        
        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveLoading(), 40)) {
                error = "Website too long to load ";
            }
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricSignOffPageObject.sighoffTab(),10)) {
            error = "Failed to click Sign Off tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.sighoffTab())) {
            error = "Failed to click Sign Off tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.sighoffSendCheckBox())) {
            error = "Failed to click send for sign off ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.auditRatingDownXpath())) {
            error = "Failed to click Audit rating dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.auditRatingXpath(getData("Audit rating")))) {
            error = "Failed to click Audit rating " + getData("Audit rating");
            return false;
        }

        narrator.stepPassed("Successfully selected Audit rating " + getData("Audit rating"));

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricSignOffPageObject.conclusionXpath(), getData("Conclusion"))) {
            error = "Failed to enter conclusion  " + getData("Conclusion");
            return false;
        }

        narrator.stepPassed("Successfully entered conclusion " + getData("Conclusion"));

        narrator.stepPassedWithScreenShot("Successfully entered send for sign off Details");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.saveRecordSignOff())) {
            error = "Failed to click Audit rating dropdown";
            return false;
        }

        return true;
    }

    public boolean auditsignOff() {

        String auditorType = getData("Auditor type");
        String auditor = getData("Auditor");
        String signOff = getData("Sign off");
        String signOffComments = getData("sign off comments");
        String email = getData("Email comments to:");
        
         if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveLoading(), 20)) {
                error = "Website too long to load ";
            }
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricSignOffPageObject.addAuditSignOff(),4000)) {
            error = "Failed to click Audit Sign Off Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.addAuditSignOff())) {
            error = "Failed to click Audit Sign Off Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.AuditSignOffAuditorTypeDownXpath())) {
            error = "Failed to click Audit Sign Off - Audit type dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.AuditSignOffAuditorTypeXpath(auditorType))) {
            error = "Failed to click Audit Sign Off - Audit type " + auditorType;
            return false;
        }
        narrator.stepPassed("Successfully selected auditor type " + auditorType);

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.AuditSignOffAuditorDownXpath())) {
            error = "Failed to click Audit Sign Off - Auditor dropdown";
            return false;
        }
SeleniumDriverInstance.pause(2500);
        if (!SeleniumDriverInstance.clickElementsbyXpath(IsoMetricSignOffPageObject.AuditSignOffAuditorTypeXpath(auditor), 6)) {
            error = "Failed to click Audit Sign Off - Auditor " + auditor;
            return false;
        }

        narrator.stepPassed("Successfully selected Auditor " + auditor);

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.AuditSignOffOffDownXpath())) {
            error = "Failed to click Audit Sign Off - Sign off dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.selectFromDropdow1n(signOff))) {
            error = "Failed to click Audit Sign Off - Sign off " + signOff;
            return false;
        }

        narrator.stepPassed("Successfully selected Sigh off " + signOff);

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricSignOffPageObject.AuditSignOffCommentsXpath(), signOffComments)) {
            error = "Failed to click Audit Sign Off - comments " + signOffComments;
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.actionFeedbackXpath(email))) {
            error = "Failed to click Audit Sign Off - Email comments to: " + email;
            return false;
        }

        narrator.stepPassed("Successfully selected Sigh off " + email);

        narrator.stepPassedWithScreenShot("Successfully entered Audit sign off details");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.saveAuditSignOff())) {
            error = "Failed to Click Audit Sign Off - Sign off button";
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveWait3(), 100)) {
                error = "Website too long to load ";
                 return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.closeAuditSignOff())) {
            error = "Failed to Click Audit Sign Off - close button";
           return false;
        }
        
         if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.laoding(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.laoding2(), 100)) {
                error = "Website too long to load 1";
                 return false;
            }
        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricSignOffPageObject.auditProtocolTabXpath())) {
            error = "Failed to Click Audit protocol tab";
            return false;
        }
        
        String auditProtocolStatus = SeleniumDriverInstance.retrieveTextByXpath(IsoMetricSignOffPageObject.auditProCompleteXpath());
        String processFlowStatus = SeleniumDriverInstance.retrieveTextByXpath(IsoMetricSignOffPageObject.processFlowXpath());
        String recordCode = SeleniumDriverInstance.retrieveTextByXpath(IsoMetricSignOffPageObject.auditRecordXpath());
        
        if(auditProtocolStatus.equalsIgnoreCase(processFlowStatus)){
            
            testData.extractParameter("Audit :", recordCode,"");
            testData.extractParameter("Audit Protocol Status :", auditProtocolStatus,"PASS");
            testData.extractParameter("Process Status :", processFlowStatus,"PASS");
        
        }else{
            testData.extractParameter("Audit :", recordCode,"");
            testData.extractParameter("Audit Protocol Status :", auditProtocolStatus,"FAIL");
            testData.extractParameter("Process Status :", processFlowStatus,"FAIL");
        }
        
        

        return true;
    }

}
