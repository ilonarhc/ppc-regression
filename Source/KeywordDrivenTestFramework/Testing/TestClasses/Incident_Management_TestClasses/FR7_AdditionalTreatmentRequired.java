/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Additional treatment required",
        createNewBrowserInstance = false
)

public class FR7_AdditionalTreatmentRequired extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR7_AdditionalTreatmentRequired() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!additionalTreatmentRequired()) {
            return narrator.testFailed("Failed fill Injured Persons - Additional treatment required - " + error);
        }

        return narrator.finalizeTest("Completed Injured Persons - Additional treatment required");
    }

    public boolean additionalTreatmentRequired() {

        if (getData("Additional treatment required").equalsIgnoreCase("True")) {

            //Check Additional Treatment Required Checkbox, Treatment away from home checkbox appears
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.AdditionalTreatmentRequiredCheckBoxXPath())) {
                error = "Failed to click on the Additional Treatment Required checkbox ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.TreatmentAwayFromWorkpaceCheckBoxXPath(), 2)) {
                error = "Failed to display Treatment away from workplace ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.TreatedInEmergencyRoomCheckBoxXPath(), 2)) {
                error = "Failed to display Treated In Emergency Room  ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.TreatedInpatientOverNightCheckBoxXPath(), 2)) {
                error = "Failed to display Treated Inpatient Over Night  ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully displayed all Additional treatment required details");

        } else if (getData("Additional treatment required").equalsIgnoreCase("false")) {

            if (!SeleniumDriverInstance.scrollToElement(InjuredPersonsPageObjects.TreatmentAwayFromWorkpaceCheckBoxXPath())) {
                error = "Falied to scroll to Treatment away from workplace ";
                return false;
            }
            narrator.stepPassedWithScreenShot(" Additional treatment required No tabs are triggered ");
        }

        return true;
    }

}
