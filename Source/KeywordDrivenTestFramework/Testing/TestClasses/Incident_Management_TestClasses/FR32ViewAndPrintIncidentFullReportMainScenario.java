/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author syotsi
 */
@KeywordAnnotation(
        Keyword = "View and Print Incident Full Report",
        createNewBrowserInstance = false
)
public class FR32ViewAndPrintIncidentFullReportMainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String path;

    public FR32ViewAndPrintIncidentFullReportMainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        String x = System.getProperty("user.dir") + "\\Incidents Reports\\Incident Full Report.pdf";

        path = new File(x).getAbsolutePath();
    }

    public TestResult executeTest() {
        if (!ViewAndPrintIncidentSummaryReport()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Passed Viewing and Printing Incident Summary Report - Main Scenario");
    }

    public boolean ViewAndPrintIncidentSummaryReport() {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentManagement_Reports_Iconxpath())) {
            error = "Failed to wait for Incident management Reports Icon.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentManagement_Reports_Iconxpath())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentManagement_Reports_Iconxpath())) {
                error = "Failed to wait for Bowtie Risk Assessment.";
                return false;
            }
            pause(9000);
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentManagement_Reports_Iconxpath())) {
                error = "Failed to click Bowtie Risk Assessment";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentManagement_IncidentFullSummary_Buttonxpath())) {
            error = "Failed to wait for Incident Summary.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentManagement_IncidentFullSummary_Buttonxpath())) {
            error = "Failed to click Incident Summary button.";
            return false;
        }

        ArrayList<String> tabs_windows = new ArrayList<String>(SeleniumDriverInstance.Driver.getWindowHandles());
        SeleniumDriverInstance.Driver.switchTo().window(tabs_windows.get(1));

        sikuliDriverUtility.pause(8000);
        narrator.stepPassedWithScreenShot("View Full report ");

        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.downloadPic())) {
            error = "Failed click download 1 image ";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.downloadPic2())) {
            error = "Failed click download 2 image ";
            return false;
        }
        File report = new File(path);

        deleteReports(report);

        if (!sikuliDriverUtility.EnterText(IncidentPageObjects.fullReportPic1(), path)) {
            error = "Failed to click image 3";
            return false;
        }

        if (!sikuliDriverUtility.MouseClickElement(IncidentPageObjects.savePic())) {
            error = "Failed to click save ";
            return false;
        }
        sikuliDriverUtility.pause(1000);
        if (!report.exists()) {
            error = "Failed to Download report";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully downloaded Report");

        return true;
    }

}
