/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;

/**
 *
 * @author MJivan
 */
@KeywordAnnotation(
        Keyword = "Incident Management",
        createNewBrowserInstance = false
)
public class IsometrixIncidentManagment extends BaseClass {

    String error = "";

    public IsometrixIncidentManagment() {

    }

    public TestResult executeTest() {

        if (!IncidentManagmentFlow()) {
            return narrator.testFailed("Failed to navigate to Isometrix Sign In Page");
        }

        return narrator.finalizeTest("Successfully Navigated through the Isometrix web page");
    }

    public boolean IncidentManagmentFlow() {

        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
            error = "Failed to switch to frame ";
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.environmentalHealthSafetyXpath())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.IncidentManagmentXpath())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 60)) {
                error = " save took long - reached the time out ";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.ViewFilterBtn())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.ViewFilterBtn())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.AddNewBtn())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.AddNewBtn())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsIncidentPageObjects.IncidentDescription(), getData("Incident Description"))) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.IncidentOccured())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Mining())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.SouthAfrica())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.VictoryMine())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Ore())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.ProjectLink())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsIncidentPageObjects.Location(), getData("Specific Location"))) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.PinToMap())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Project())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Simon())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.SelectMap())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(IsoMetricsIncidentPageObjects.RiskDisciple())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.RiskDisciple())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.incidenttypeAll())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Compliance())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.SelectAll())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.IncidentClassification())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Accident())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Environmental())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Injury())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.ClickUp())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.DatePicker())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Date())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsIncidentPageObjects.IncidentTime(), getData("Time"))) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Shift())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.DayShift())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsIncidentPageObjects.ActionTaken(), getData("Action"))) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Reported())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.SuperVisor())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.SelectAdmin())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.Save())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        return true;
    }
}
