/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.Keys;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Return To Work Management Doctor's Notes and Save",
        createNewBrowserInstance = false
)

public class WorkManagementDoctorsNotesAnsSave extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public WorkManagementDoctorsNotesAnsSave()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest()
    {
        if (!DoctorsNotesDetails())
        {
            return narrator.testFailed("Failed to enter Doctor's notes details - " + error);
        }
        if (!ValidateRecordIsSaved())
        {
            return narrator.testFailed("Failed to validate that the record is saved - " + error);
        }
        return narrator.finalizeTest("Successfully saved an Work Management record");
    }

    public boolean DoctorsNotesDetails()
    {
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.MedicalPractionerDropdownXPath()))
        {
            error = "Failed clicked on the Link Case To Incident Record dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Medical Practionar"))))
        {
            error = "Failed to select the following item: " + getData("Medical Practionar");
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.DoctorsNotesTextAreaXPath(), getData("Doctors Notes")))
        {
            error = "Failed to fill in the Doctor's Notes text area";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.DateIssuedXPath(), date))
        {
            error = "Failed to enter text into the Date Issued text field";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.FitnessForWorkDropdownXPath()))
        {
            error = "Failed clicked on the Link Case To Incident Record dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Fitness"))))
        {
            error = "Failed to select the following item: " + getData("Fitness");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.DateFromXPath(), date))
        {
            error = "Failed to enter text into the Date From text field ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.RestrictionsTextAreaXPath()))
        {
            error = "Failed to wait for the Restrictions text area";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(MainScenario_PageObjects.RestrictionsTextAreaXPath(), getData("Restrictions")))
        {
            error = "Failed to enter text for the Restrictions text area";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.DateToXPath()))
        {
            error = "Failed to wait for the Date To text field";
            return false;
        }
        try
        {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date currentDate = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(currentDate);
            cal.add(Calendar.DATE, 1);
            Date currentDatePlusOne = cal.getTime();

            if (!SeleniumDriverInstance.enterTextByXpath(InjuredPersonsPageObjects.DateToXPath(), dateFormat.format(currentDatePlusOne)))
            {
                error = "Failed to enter text into the Date To text field";
                return false;
            }
            SeleniumDriverInstance.pressKey(Keys.ENTER);
        } catch (Exception e)
        {
            error = "Failed to get the current date - " + e.getMessage();
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully validated that the Restrictions and Date To fields are displayed");

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.SaveFormButtonXPath()))
        {
            error = "Failed to click on the Doctor's Notes Heading";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 400))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.CloseForm2ButtonXPath()))
        {
            error = "Failed to click on the close form Button ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 4))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        return true;
    }

    public boolean ValidateRecordIsSaved()
    {
        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.VerificationAndAdditionalTab(),10))
        {
            error = "Failed to wait for the Verification And Additional Tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.VerificationAndAdditionalTab()))
        {
            error = "Failed to click on the Verification And Additional Tab ";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.WaitForItemXPath(getData("Full Name")),40))
        {
            error = "Failed to verify that the Incident Management record was added";
            return false;
        }
        narrator.stepPassedWithScreenShot("Record is saved - " + getData("Full Name"));
        return true;
    }
}
