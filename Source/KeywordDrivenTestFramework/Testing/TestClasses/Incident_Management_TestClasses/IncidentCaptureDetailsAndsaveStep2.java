/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.recordId;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import org.openqa.selenium.By;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Save and Continue to step 2",
        createNewBrowserInstance = false
)
public class IncidentCaptureDetailsAndsaveStep2 extends BaseClass {

    String error = "";

    public IncidentCaptureDetailsAndsaveStep2() {

    }

    public TestResult executeTest() {

        if (!captureDetailsAndsaveStep2()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed Save and Continue to step 2");
    }

    public boolean captureDetailsAndsaveStep2() {

        if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.saveStep2())) {
            error = "Failed to click to Save and continue to Step 2";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        String[] record = SeleniumDriverInstance.Driver.findElement(By.xpath(IncidentPageObjects.getRecordIdXpath())).getText().split("#");
        setRecordId(record[1]);
        
        if (!SeleniumDriverInstance.scrollToElement(IsoMetricsIncidentMainScenarioPageObject.addDetailsXpath())) {
            error = "Failed to Save and continue to Step 2";
            return false;
        }
        
        

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentMainScenarioPageObject.addDetailsXpath())) {
            error = "Failed to click to Save and continue to Step 2";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.personInvolvedXpath(), 2)) {
            error = "Failed to display Persons Involved";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.witnessStatementsXpath(), 2)) {
            error = "Failed to display witness Statements";
            return false;
        }
        
        setRecordId(record[1]);

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.euipmentInvolvedTabXpath(), 2)) {
            error = "Failed to display Equipment Involved";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.Safety(), 2)) {
            error = "Failed to display Safety tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.EnvironmentXpath(), 2)) {
            error = "Failed to display Environment tab";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.QualitytXpath(), 2)) {
//            error = "Failed to display Quality tab";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.railwaySafetyXpath(), 2)) {
//            error = "Failed to display Railway Safety tab";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.socialSusXpath(), 2)) {
            error = "Failed to display Social Sustainability tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.occHygieneXpath(), 2)) {
            error = "Failed to display Occupational Hygiene tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.complianceXpath(), 2)) {
            error = "Failed to display Compliance tab";
            return false;
        }

        narrator.stepPassed("Successfully saved and continue to step 2 ");

        return true;
    }
}
