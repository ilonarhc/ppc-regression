/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.GmailPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsCaptureAuditsPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsScheduleAuditPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsometricsRiskAssessmentScopePageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author MJivan
 */
@KeywordAnnotation(
        Keyword = "Risk Assessment Scope",
        createNewBrowserInstance = false
)
public class IsoMetricsRiskAssessmentScope extends BaseClass {

    String error = "";

    public IsoMetricsRiskAssessmentScope() {

    }

    public TestResult executeTest() {

        if (!navigateToRiskAssessment()) {
            return narrator.testFailed("Failed to navigate Risk Assessment scope Page - " + error);
        }

        return narrator.finalizeTest("Successfully Saved Audit record ");
    }

    public boolean navigateToRiskAssessment() {

        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsCaptureAuditsPageObject.iframeXpath())) {
            error = "Failed to switch to frame ";
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.environmentalHealthSafetyXpath())) {
            error = "Failed to click Environmental Health & safety button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.riskManagementButtonXpath())) {
            error = "Failed to click risk Management button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.integratedRiskRegisterButtonXpath())) {
            error = "Failed to click integrated RiskRegister button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.riskManagementAddButtom())) {
            error = "Failed to click risk Management Add button";
            return false;
        }

        String dateOfAssessment = getData("Date of assessment");

        if (!SeleniumDriverInstance.enterTextByXpath(IsometricsRiskAssessmentScopePageObjects.rmDateOfAssXpath(), dateOfAssessment)) {
            error = "Failed to enter Date of assessment " + dateOfAssessment;
            return false;
        }

        narrator.stepPassed("SuccessFully entered Date of assessment : " + dateOfAssessment);

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.businessUnitdropDownXpath())) {
            error = "Failed to click business unit dropdown";
            return false;
        }

        String[] units = getData("Business Unit").split("->");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.rmBusinessUnitXpath(units[0]))) {
            error = "Failed to click business unit - " + units[0];
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.rmBusinessUnitXpath(units[1]))) {
            error = "Failed to click business unit - " + units[1];
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.rmBusinessUnitXpath(units[2]))) {
            error = "Failed to click business unit - " + units[2];
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.businessUnit(units[3]))) {
            error = "Failed to click business unit - " + units[3];
            return false;
        }

        narrator.stepPassed("SuccessFully selected business unit : " + getData("Business Unit"));

        String[] discipline = getData("Risk Discipline").split(",");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.riskdisciplinedDownXpath())) {
            error = "Failed to click risk discipline dropdown";
            return false;
        }

        for (int k = 0; k < discipline.length; k++) {
            if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.riskdiscipline(discipline[k]))) {
                error = "Failed to click Risk discipline - " + units[0];
                return false;
            }
            narrator.stepPassed("SuccessFully checked Risk discipline  : " + discipline[k]);
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.nothing())) {
            error = "Failed to click risk discipline nothing";
            return false;
        }

        String riskAssessmentRef = getData("Risk assessment reference");

        if (!SeleniumDriverInstance.enterTextByXpath(IsometricsRiskAssessmentScopePageObjects.riskAssessmentRef(), riskAssessmentRef)) {
            error = "Failed to enter Risk assessment reference " + riskAssessmentRef;
            return false;
        }

        narrator.stepPassed("SuccessFully entered Risk assessment reference : " + riskAssessmentRef);

        String scope = getData("Scope");

        if (!SeleniumDriverInstance.enterTextByXpath(IsometricsRiskAssessmentScopePageObjects.scopeTextareaXpath(), scope)) {
            error = "Failed to enter Risk assessment scope " + scope;
            return false;
        }

        narrator.stepPassed("SuccessFully entered Risk assessment scope : " + scope);

        String scopeCate = getData("Scope category");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.scopeCategoryDropDownXpath())) {
            error = "Failed to select Scope category dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.scopeCategory(scopeCate))) {
            error = "Failed to select Scope category " + scopeCate;
            return false;
        }

        narrator.stepPassed("SuccessFully selected Scope category : " + scopeCate);

        String businessPro = getData("Business processes");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.riskdiscipline(businessPro))) {
            error = "Failed to select Business processes " + businessPro;
            return false;
        }

        narrator.stepPassed("SuccessFully selected Business processes : " + businessPro);

        String responsiblePerson = getData("Responsible person");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.responsiblePersonDropdownXpath())) {
            error = "Failed to click Responsible person dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.scopeCategory(responsiblePerson))) {
            error = "Failed to click Responsible person " + responsiblePerson;
            return false;
        }

        narrator.stepPassed("SuccessFully selected Responsible person : " + responsiblePerson);

        String RiskAssessmentfrom = getData("Create risk assessment from");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.CreateRiskAssessmenDropdownXpath())) {
            error = "Failed to click Create risk assessment from dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.scopeCategory(RiskAssessmentfrom))) {
            error = "Failed to click Create risk assessment from " + RiskAssessmentfrom;
            return false;
        }

        narrator.stepPassed("SuccessFully selected Create risk assessment from : " + RiskAssessmentfrom);

        String[] riskSources = getData("Risk source").split(",");

        for (int k = 0; k < 3; k++) {

            if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.risksource(riskSources[k]))) {
                error = "Failed to click Risk source dropdown " + riskSources[k];
                return false;
            }

        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.riskdiscipline(riskSources[3]))) {
            error = "Failed to click Risk source dropdown " + riskSources[3];
            return false;
        }

        if (!SeleniumDriverInstance.clickElementsbyXpath(IsometricsRiskAssessmentScopePageObjects.riskdiscipline1(riskSources[4]), 0)) {
            error = "Failed to click Risk source dropdown " + riskSources[3];
            return false;
        }

        if (!SeleniumDriverInstance.clickElementsbyXpath(IsometricsRiskAssessmentScopePageObjects.riskdiscipline1(riskSources[4]), 1)) {
            error = "Failed to click Risk source dropdown " + riskSources[3];
            return false;
        }

        narrator.stepPassedWithScreenShot("Complete Risk Assessment scope");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsRiskAssessmentScopePageObjects.saveButtonXpath())) {
            error = "Failed to click Risk assessment scope ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveWait2(), 200)) {
                error = "Website too long to load ";
            }
        }
        
        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsRiskAssessmentScopePageObjects.riskSourceXpath())) {
            error = "Failed to wait for risk soucre";
        }

        List<WebElement> list = SeleniumDriverInstance.Driver.findElements(By.xpath(IsometricsRiskAssessmentScopePageObjects.riskSourceXpath()));

        testData.extractParameter("Risk Sources", "", "");
        for (int k = 0; k < list.size(); k++) {

            testData.extractParameter(" " + k, SeleniumDriverInstance.retrieveTextByXpath(IsometricsRiskAssessmentScopePageObjects.tableRiskSource(k + 1)), "PASS");

        }

        return true;
    }

}
