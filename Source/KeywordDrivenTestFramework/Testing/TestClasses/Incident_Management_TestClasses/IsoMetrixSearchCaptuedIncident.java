/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Search",
        createNewBrowserInstance = false
)
public class IsoMetrixSearchCaptuedIncident extends BaseClass {

    String error = "";

    public IsoMetrixSearchCaptuedIncident() {

    }

    public TestResult executeTest() {

        if (!loggingAnIncident()) {
            return narrator.testFailed("Failed to logging An Incident due - " + error);
        }

        return narrator.finalizeTest("Successfully logging An Incident");
    }

    public boolean loggingAnIncident() {

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
            error = "Failed to switch to frame ";
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
            error = "Failed to switch to frame ";
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.environmentalHealth())) {
            error = "Failed to wait for Environmental Health Safety";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.environmentalHealth())) {
            error = "Failed to click Environmental Health Safety";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.IncidentManagmentXpath())) {
            error = "Failed to wait for Incident Managment";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.IncidentManagmentXpath())) {
            error = "Failed to click Incident Managment";
            return false;
        }

//        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.IncidentManagemetLoadingXpath(), 5)) {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsIncidentPageObjects.IncidentManagemetLoadingDoneXpath(), 70)) {
//                error = " save took long - reached the time out 1";
//                return false;
//            }
//        }else{
//            System.out.println("NOT Found");
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.AddNewBtn(), 40)) {
            error = "Failed to click add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.seacrhButton())) {
            error = "Failed to click Search button";
            return false;
        }

        String record = getData("RecordID");
        String previous = getData("Previous RecordID");
//        boolean nextTable = false;
//
//        List<WebElement> allID = SeleniumDriverInstance.Driver.findElements(By.xpath(IsoMetricsIncidentPageObjects.allRecordID()));
//
//        for (WebElement element : allID) {
//            if (element.getText().equalsIgnoreCase(record)) {
//                nextTable = true;
//            } else {
//                if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.recordXpath(record))) {
//                    error = "Failed to click to incident management record id : " + getRecordId();
//                    return false;
//                }
//
//            }
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.ViewFilter_Button())) {
            error = "Failed to wait for View filter button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.ViewFilter_Button())) {
            error = "Failed to click View filter button";
            return false;
        }

        pause(1000);

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.ViewFilter__RecordNumber_textfield())) {
            error = "Failed to wait for View filter Record number search";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsIncidentPageObjects.ViewFilter__RecordNumber_textfield(), record)) {
            error = "Failed to entertext into View filter Record number: " + record;
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.ViewFilter_Search_Button())) {
            error = "Failed to wait for Seach button.";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.ViewFilter_Search_Button())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.ViewFilter_Search_Button())) {
                error = "Failed to wait for Seach button.";
                return false;
            }
            pause(5000);
            if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.ViewFilter_Search_Button())) {
                error = "Failed to click Search button.";
                return false;
            }
        }

        pause(1000);

        if (previous.isEmpty() || previous.equalsIgnoreCase("true") && !record.isEmpty()) {

            if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.recordXpath(record), 20)) {
                error = "Failed to wait to incident management record id : " + record;
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.recordXpath(record))) {
                error = "Failed to click to incident management record id : " + getRecordId();
                return false;
            }

        } else if (previous.isEmpty() || previous.equalsIgnoreCase("false") && record.isEmpty()) {

            if (!SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.recordXpath(getRecordId()), 20)) {
                error = "Failed to wait to incident management record id : " + getRecordId();
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.recordXpath(getRecordId()))) {
                error = "Failed to click to incident management record id : " + getRecordId();
                return false;
            }
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActiveSearch(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingFormsActiveSearchDone(), 40)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingDataIncidenrSearch(), 60)) {
                error = " save took long - reached the time out ";
                return false;
            }
        }

        narrator.stepPassed("Successfully record found ");

        return true;
    }
}
