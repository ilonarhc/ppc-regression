/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Injured Persons Main Scenario",
        createNewBrowserInstance = false
)

public class FR7_CaptureInjuredPersonsMainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;

    public FR7_CaptureInjuredPersonsMainScenario() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
    }

    public TestResult executeTest() {
        if (!injuredPersons()) {
            return narrator.testFailed("Failed fill Injured Persons - " + error);
        }

        return narrator.finalizeTest("Completed Injured Persons");
    }

    public boolean injuredPersons() {
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.addDetailsXpath())) {
            error = "Failed to wait for 2.Verification and Additional Detail";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.addDetailsXpath())) {
            error = "Failed to click 2.Verification and Additional Detail";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.safetyTab())) {
            error = "Failed to wait for the safety tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.safetyTab())) {
            error = "Failed to click the safety tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.addInjuredPersonsButtonXPath())) {
            error = "Failed to click the Add button ";
            return false;
        }
         SeleniumDriverInstance.pause(1500);

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 40)) {
                error = "Webside too long to load wait reached the time out save";
                return false;
            }
        }
        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 1)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40)) {
                error = "Webside too long to load wait reached the time out loading permission";
            }
        }

       

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.injuredPersonsFlow())) {
            error = "Fail to click Injured persons process flow";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.employeeTypeDropdownXPath())) {
            error = "Failed to click the employee type dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.anySupervisorXpath(getData("Employee")))) {
            error = "Failed to click the employee type element: " + testData.getData("Employee");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.fullNameDropdownXPath())) {
            error = "Failed to click the full name dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.anySupervisorXpath(testData.getData("Full Name")))) {
            error = "Failed to click the full name element: " + testData.getData("Full Name");
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.illnessonduty_dropown())){
            error = "Failed to wait for Illness on duty dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.illnessonduty_dropown())){
            error = "Failed to click Illness on duty dropdown.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(InjuredPersonsPageObjects.illnessonduty_select(testData.getData("On Duty")))){
            error = "Failed to wait for Illness on duty option '"+testData.getData("On Duty")+"'.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.illnessonduty_select(testData.getData("On Duty")))){
            error = "Failed to click Illness on duty option '"+testData.getData("On Duty")+"'.";
            return false;
        }
        

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        return true;
    }

}
