/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuryClaimPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR10 Compensation claim",
        createNewBrowserInstance = false
)

public class FR10_IsthisCompensationClaim extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    InjuredPersons injuredPersons;
    String date;

    public FR10_IsthisCompensationClaim() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        injuredPersons = new InjuredPersons();
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!compensationClaim()) {
            return narrator.testFailed("Failed fill Is this a compensation claim " + error);
        }

        return narrator.finalizeTest("Completed Is this a compensation claim ");
    }

    public boolean compensationClaim() {

        if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.workersTabCheckOpenXpath(), 2)) {
            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.workersTabXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details  ";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.employeeDropdownXpath())) {
            error = "Failed to click injury Claim - Worker's Personal Details - Employess dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.responsiblePerson(getData("Employee")))) {
            error = "Failed to click injury Claim - Worker's Personal Details - Employess  ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.languageDropdownXpath())) {
            error = "Failed to click injury Claim - Worker's Personal Details - language dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.responsiblePerson(getData("Communication Disability")))) {
            error = "Failed to click injury Claim - Worker's Personal Details - language  ";
            return false;
        }
        
         if (!SeleniumDriverInstance.enterTextByXpath(InjuryClaimPageObject.communicationDisability(),getData("special communication"))) {
            error = "Failed to enter injury Claim - Worker's Personal Details - Do you have special communication needs because of disability? ";
            return false;
        }

        String compensationClaim = getData("Is this a compensation claim");

        if (compensationClaim.equalsIgnoreCase("Yes")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.compensationClaimDropdownXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - compensation Claim dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.anySupervisorXpath(compensationClaim))) {
                error = "Failed to click injury Claim - Worker's Personal Details - compensation claim  " + compensationClaim;
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.partnerDropDownXpath(), 2)) {
                error = "Failed to display injury Claim - Worker's Personal Details - Do you support a partner? ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.fullTimeStudentsXpath(), 2)) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support any children under the age of 18 or full-time students? ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Is this a compensation claim and 2 fields are displayed.");

        } else if (compensationClaim.equalsIgnoreCase("No")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.compensationClaimDropdownXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - compensation Claim dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(InjuryClaimPageObject.anySupervisorXpath(compensationClaim))) {
                error = "Failed to click injury Claim - Worker's Personal Details - compensation claim  " + compensationClaim;
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(InjuryClaimPageObject.fullTimeStudentsXpath(), 2)) {
                error = " Worker's Personal Details - Do you support any children under the age of 18 or full-time students fields are triggered ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Is this a compensation claim and No fields are triggered. ");
        }

        return true;
    }

}
