/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Regulatory Authority Alternative Scenario 2",
        createNewBrowserInstance = false
)

public class FR6_Capture_Regulatory_Authority_AlternativeScenario2 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR6_Capture_Regulatory_Authority_AlternativeScenario2()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!safety())
        {
            return narrator.testFailed("Failed to navigate to Safety in order to validate that the Regulatoy Authority panel is not displayed - " + error);
        }

        return narrator.finalizeTest("Successfully validated that the Regulatory Authority panel is not displayed when 'Is this event reportable to any authority and if reportable to which regulatory authority do you need to report this event?' is not ticked");
    }

    public boolean safety()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.safetyTab()))
        {

            error = "Failed to wait for the safety tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.safetyTab()))
        {
            error = "Failed to click the safety tab";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.safetyRegulatoryAuthorityHeadingXpath()))
        {
            error = "Failed to wait for the Safety Regulatory Authority heading";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully validated that the Regulatory Authority panel is not displayed");
        return true;
    }

}
