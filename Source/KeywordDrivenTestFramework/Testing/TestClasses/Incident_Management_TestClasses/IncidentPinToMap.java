/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsCaptureAuditsPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsScheduleAuditPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.By;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Pin to map",
        createNewBrowserInstance = false
)
public class IncidentPinToMap extends BaseClass {

    String error = "";

    public IncidentPinToMap() {

    }

    public TestResult executeTest() {

        if (!pinMap()) {
            return narrator.testFailed("Failed to pin to a map due - " + error);
        }

        return narrator.finalizeTest("Completed pin to a map");
    }

    public boolean pinMap() {

        SikuliDriverUtility sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");

        if (getData("Pin to Map").equalsIgnoreCase("True")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.PinToMap())) {
                error = "Failed to click  Pin to Map";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentPageObjects.SelectMap())) {
                error = "Failed to click Map";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.mapOpenTab())) {
                error = "Failed to validate Map screen";
                return false;
            }

            narrator.stepPassed("Successfully pin to map");
        } else {
//
//            if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentMainScenarioPageObject.incidentReport())) {
//                error = "Failed to click 1.Incident Report tab";
//                return false;
//            }

            if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.pinMapChecked(),1)) {
                if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.PinToMap())) {
                    error = "Failed to Unchecke Pin to Map checkbox";
                    return false;
                }
            }

           if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.mapCloseTab(),1)) {
                error = "Failed to close map ";
                return false;
            }
            
            
             narrator.stepPassed("Do not tick the Pin to map checkbox.");

        }

        return true;
    }
}
