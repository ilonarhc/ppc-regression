/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.Keys;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Return To Work Management Alternate Scenario 3",
        createNewBrowserInstance = false
)

public class FR8_Capture_Return_To_Work_Management_AlternateScenario3 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public FR8_Capture_Return_To_Work_Management_AlternateScenario3()
    {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest()
    {
        if (!ReturnToWorkManagement())
        {
            return narrator.testFailed("Failed to validate that'Chronic Illness' panel is displayed - " + error);
        }
        return narrator.finalizeTest("'Chronic Illness' panel is displayed");
    }

    public boolean ReturnToWorkManagement()
    {

        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.ReturnToWorkManagementXPath(), 500))
        {
            error = "Failed to scroll to the Return To Work Mangement heading ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.ReturnToWorkManagementAddButtonXPath()))
        {
            error = "Failed to click the Add button ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingFormsActive(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.loadingPermissionActive(), 1))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingPermissions(), 40))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.StatusDropdownXPath()))
        {
            error = "Failed to click the Status Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(testData.getData("Status"))))
        {
            error = "Failed to select the Reportable to item: " + testData.getData("Status");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.OriginOfCaseDropdownXPath()))
        {
            error = "Failed to click on the dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Origin Of Case"))))
        {
            error = "Failed to click the Origin Of Case Dropdown";
            return false;
        }
         if (!SeleniumDriverInstance.waitForElementsByXpath(InjuredPersonsPageObjects.ManagementWhoReferredThePersonXPath()))
        {
            error = "Failed to validate that 'Management who referred the person' field is not displayed";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementsByXpath(InjuredPersonsPageObjects.TestToBeConductedXPath()))
        {
            error = "Failed to validate that 'Test To Be Conducted' field is not displayed";
            return false;
        }
        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.ChronicIllnessPanelXPath()))
        {
            error = "Failed to move to the 'Chronic Illness' panel ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementsByXpath(InjuredPersonsPageObjects.ChronicIllnessPanelXPath()))
        {
            error = "Failed to validate that 'Chronic Illness' panel is displayed";
            return false;
        }
        

        narrator.stepPassedWithScreenShot("'Chronic Illness' panel is displayed");
        return true;
    }

    

    
}
