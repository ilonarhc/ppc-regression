/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.SikuliDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.GmailPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsCaptureAuditsPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsScheduleAuditPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author MJivan
 */
@KeywordAnnotation(
        Keyword = "Capture Audit",
        createNewBrowserInstance = false
)
public class IsoMetricsCaptureAudit extends BaseClass {

    String error = "";

    public IsoMetricsCaptureAudit() {

    }

    public TestResult executeTest() {

        if (!navigatingToAuditPage()) {
            return narrator.testFailed("Failed to navigate Audit Page - " + error);
        }

        if (!auditProtocol()) {
            return narrator.testFailed("Failed to navigate Audit Page - " + error);
        }

        return narrator.finalizeTest("Successfully Saved Audit record ");
    }

    public boolean navigatingToAuditPage() {

        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsCaptureAuditsPageObject.iframeXpath())) {
            error = "Failed to switch to frame ";
        }
        
      
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.environmentalHealthSafetyXpath())) {
            error = "Failed to click Environmental, Health and Safety Button";
            return false;
        }
        narrator.pause((1000));

        narrator.stepPassedWithScreenShot("Successfully Navigated Environmental, Health and Safety page ");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.auditsInspectionsXpath())) {
            error = "Failed to click Audits Inspections Button";
            return false;
        }
        narrator.pause((1000));
        narrator.stepPassedWithScreenShot("Successfully Navigated Audits & Inspections page ");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.auditsXpath())) {
            error = "Failed to click Audit Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.addButtonXpath())) {
            error = "Failed to click add Button";
            return false;
        }

        return true;
    }

    public boolean auditProtocol() {

        String businessUnit = getData("Business Unit");
        SikuliDriverInstance = new SikuliDriverUtility(IsoMetricsCaptureAuditsPageObject.SikuliImagePath());

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsScheduleAuditPageObject.flowProcessubuttonXpath())) {
            error = "Failed to check Flow Process button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsScheduleAuditPageObject.pinProcessXpath())) {
            error = "Failed to pin Flow Process button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Logging audit");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.businessdownButtonXpath())) {
            error = "Failed to click business dropdown Button ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.miningXpath())) {
            error = "Failed to click Mining dropdown Button ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.southAfricaXpath())) {
            error = "Failed to click South Africa dropdown Button ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.victoryMineXpath())) {
            error = "Failed to click Victory Mine dropdown Button ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.oraProcessingXpath())) {
            error = "Failed to click Ora Processing dropdown Button ";
            return false;
        }
        narrator.stepPassed("Succesfully selected " + SeleniumDriverInstance.retrieveTextByXpath(IsoMetricsCaptureAuditsPageObject.unitTextXpath()));

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.projectCheckBoxXpath())) {
            error = "Failed to check  project link Checkbox ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.projectdropdownXpath())) {
            error = "Failed to click project dropdown Checkbox ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.proProjectTestXpath())) {
            error = "Failed to select project ";
            return false;
        }
        narrator.stepPassed("Succesfully selected " + SeleniumDriverInstance.retrieveTextByXpath(IsoMetricsCaptureAuditsPageObject.projectTextXpath()));

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.relatedStakeholderdropdownXpath())) {
            error = "Failed to click related stakeholder dropdown  ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.stakeHolderAmaDwalaXpath())) {
            error = "Failed to select Amadwala Trading 38 CC ";
            return false;
        }
        String stakeHolders = SeleniumDriverInstance.retrieveTextByXpath(IsoMetricsCaptureAuditsPageObject.stakeHoldersTextXpath());
        narrator.stepPassed("Succesfully selected " + stakeHolders + " Ralated Stakeholder");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.riskdisciplinedDownXpath())) {
            error = "Failed to click risk discipline dropdown  ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.complianceCheckBoxXpath())) {
            error = "Failed to check compliance CheckBox ";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(IsoMetricsCaptureAuditsPageObject.SafetyCheckBoxXpath())) {
            error = "Failed to check Safety CheckBox ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.SafetyCheckBoxXpath())) {
            error = "Failed to check risk discipline dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.riskdisciplinedUpXpath())) {
            error = "Failed to click risk discipline dropdown  ";
            return false;
        }

        String name = "";
        List<WebElement> risk = SeleniumDriverInstance.Driver.findElements(By.xpath(IsoMetricsCaptureAuditsPageObject.riskTextXpath()));
        for (WebElement element : risk) {
            name = name + " " + element.getText();
        }

        narrator.stepPassed("Succesfully selected " + name + " Risk discipline");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.auditTypeDownXpath())) {
            error = "Failed to click Audit Type dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.auditTypeInternalXpath())) {
            error = "Failed to click Audit Type ";
            return false;
        }

        narrator.stepPassed("Succesfully selected " + SeleniumDriverInstance.retrieveTextByXpath(IsoMetricsCaptureAuditsPageObject.auditTypeTextXpath()));

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.auditProtocolDownXpath())) {
            error = "Failed to click Audit Protocol dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.Iso9001Xpath())) {
            error = "Failed to click ISO 9001 Internal Audit  ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.humanResXpath())) {
            error = "Failed to click Human Resource  ";
            return false;
        }

        narrator.stepPassed("Succesfully selected " + SeleniumDriverInstance.retrieveTextByXpath(IsoMetricsCaptureAuditsPageObject.auditProtocolTextXpath()));

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.businessProcessAFXpath())) {
            error = "Failed to click Bussiness Process Accounts & Finance";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.businessProcessAMpath())) {
            error = "Failed to click Bussiness Process Account Maintenance  ";
            return false;
        }

        narrator.stepPassed("Succesfully selected " + SeleniumDriverInstance.retrieveTextByXpath(IsoMetricsCaptureAuditsPageObject.businessProTextXpath()));

        
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
       
        Calendar cal = Calendar.getInstance();
        
        startDate = sdf.format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH,3);
        
        endDate = sdf.format(cal.getTime());
        
        
        
        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsCaptureAuditsPageObject.strDateXpath(), startDate)) {
            error = "Failed to enter  audit start date  ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsCaptureAuditsPageObject.strEndDate(),endDate)) {
            error = "Failed to enter  audit end date  ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.auditManagerDownXpath())) {
            error = "Failed to click audit manager dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.selectFromDropdown(getData("Audit manager")))) {
            error = "Failed to click Auditee ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.auditDownXpath())) {
            error = "Failed to click person conducting the Audit dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.selectFromDropdown(getData("Auditee")))) {
            error = "Failed to click person conducting the Audit";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.personCondAuditDownXpath())) {
            error = "Failed to click person conducting the Audit";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.selectFromDropdown(getData("Person conducting the audit")))) {
            error = "Failed to click person conducting the Audit";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsCaptureAuditsPageObject.auditReferenceText(), "Test Automation")) {
            error = "Failed to enter  audit Reference ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsCaptureAuditsPageObject.ntroductionText(), "Test Automation")) {
            error = "Failed to enter  introduction ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsCaptureAuditsPageObject.auditObjectiveText(), "Test Automation")) {
            error = "Failed to enter  audit objective ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsCaptureAuditsPageObject.auditScopeText(), "Test Automation")) {
            error = "Failed to enter  audit objective ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.applicableSectionAuditsXpath())) {
            error = "Failed to click ISO 14001";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.performanceReXpath())) {
            error = "Failed to click 4. Performance Evaluation";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.verifyOMAPXpath())) {
            error = "Failed to click 4.1 Verify organization is monitoring, measuring, analyzing and evaluating its environmental performance";
            return false;
        }
        String section = "";
        List<WebElement> sections = SeleniumDriverInstance.Driver.findElements(By.xpath(IsoMetricsCaptureAuditsPageObject.section()));
        for (WebElement element : sections) {
            section = section + " " + element.getText();
        }
        narrator.stepPassed("Succesfully selected " + section + " Applicable sections to be audited");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.saveXpath())) {
            error = "Failed to click save button";
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        return true;
    }

}
