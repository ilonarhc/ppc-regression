/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Reported by",
        createNewBrowserInstance = false
)
public class IncidentReportedBy extends BaseClass {

    String error = "";

    public IncidentReportedBy() {

    }

    public TestResult executeTest() {

        if (!reportedBy()) {
            return narrator.testFailed("Failed to Reported by due - " + error);
        }

        return narrator.finalizeTest("Completed Reported by");
    }

    public boolean reportedBy() {

        if (getData("Reported by").equalsIgnoreCase("True")) {

            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.Reported())) {
                error = "Failed to click Reported by check box";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.reportedByAc(userName))) {
                error = "Failed to automatically populated in the Reported by field ";
                return false;
            }

            narrator.stepPassed("Automatically populated in the Reported by field Successfully ");
        } else {

//            if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.reportedByChecked(),2)) {
//                if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.Reported())) {
//                    error = "Failed to Uncheck Reported by checkbox";
//                    return false;
//                }
//            }
            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.SuperVisor())) {
                error = "Failed to click Reported by dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(IncidentPageObjects.incident_Select(getData("Reported by")))) {
                error = "Failed to click Reported by projecte User";
                return false;
            }

            narrator.stepPassedWithScreenShot("Do not tick the Reported by checkbox.");

        }

        return true;
    }
}
