/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsCaptureAuditsPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsScheduleAuditPageObject;

/**
 *
 * @author MJivan
 */
@KeywordAnnotation(
        Keyword = "Schecule",
        createNewBrowserInstance = false
)
public class IsoMetricsScheduleAuditRecord extends BaseClass {

    String error = "";

    public IsoMetricsScheduleAuditRecord() {

    }

    public TestResult executeTest() {

        if (!scheduled()) {
            return narrator.testFailed("Failed to scheduled  - " + error);
        }

        return narrator.finalizeTest("Successfully Schedulated Audit");
    }

    public boolean scheduled() {

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsScheduleAuditPageObject.newTimeCheckBoxXpath())) {
            error = "Failed to check new time";
            return false;
        }
        narrator.stepPassedWithScreenShot("Planned Audit");

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsScheduleAuditPageObject.proStrtDateXpath(), startDate)) {
            error = "Failed to enter Suggested start date ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsScheduleAuditPageObject.proEndDateXpath(), endDate)) {
            error = "Failed to enter Suggested start end ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsScheduleAuditPageObject.proCommentsXpath(), getData("Commits"))) {
            error = "Failed to enter commnets start end ";
            return false;
        }

        

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsScheduleAuditPageObject.accteptTimeCheckBoxXpath())) {
            error = "Failed to click Please note that the audit manager must log in to accept the proposed time";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsCaptureAuditsPageObject.saveXpath())) {
            error = "Failed to click save button";
            return false;
        }

            if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 2)) {
                if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveWait2(), 100)) {
                    error = "Website too long to load ";
                    return false;
                }
            }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.valiadteScheduleXpath())) {
            error = "Failed to schedule Audit Record";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("S Audit");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsScheduleAuditPageObject.auditTeambuttonXpath())) {
            error = "Failed to click Audit Team tab ";
            return false;
        }

         SeleniumDriverInstance.pause(4000);
       
        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsScheduleAuditPageObject.AddButtonXpath())) {
            error = "Failed to click Add button";
            return false;
        }
        
        

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsScheduleAuditPageObject.fullNameDownXpath())) {
            error = "Failed to click Full name dropdown";
            return false;
        }
        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsScheduleAuditPageObject.fullName(getData("full Name")))) {
            error = "Failed to click Full name ";
            return false;
        }

        narrator.stepPassed("Successfully selected " + getData("full Name"));

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsScheduleAuditPageObject.roleXpath(), getData("Role"))) {
            error = "Failed to enter Role";
            return false;
        }

        narrator.stepPassed("Successfully entered  Role" + getData("Role"));

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsScheduleAuditPageObject.teamStrtDateXpath(), startDate)) {
            error = "Failed to enter Audit Team Start date ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(IsoMetricsScheduleAuditPageObject.teameEndDateXpath(), endDate)) {
            error = "Failed to enter Audit Team end date ";
            return false;
        }

        narrator.stepPassed("Successfully entered  Audit Team Start date " + startDate + " And end date " + endDate);

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsScheduleAuditPageObject.saveTeam())) {
            error = "Failed to click save Team";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(IsoMetricsScheduleAuditPageObject.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IsoMetricsScheduleAuditPageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        return true;
    }

    public boolean AuditTeam() {

        return true;
    }

}
