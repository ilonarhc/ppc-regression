/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.EnvironmentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.VerificationAndAdditionalPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR13 Environment pill",
        createNewBrowserInstance = false
)

public class FR13_EnvironmentPill extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public FR13_EnvironmentPill() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!EnvironmentSpill()) {
            return narrator.testFailed("Failed to fill out the Environment Spill details - " + error);
        }

        return narrator.finalizeTest("Successfully Record is saved and automatically moves to the Edit phase");
    }

    public boolean EnvironmentSpill() {
        
         if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentMainScenarioPageObject.addDetailsXpath(),10)) {
            error = "Failed to wait to Save and continue to Step 2";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(IsoMetricsIncidentMainScenarioPageObject.addDetailsXpath())) {
            error = "Failed to click to Save and continue to Step 2";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.EnvironmentTabXPath())) {
            error = "Failed to click on the Environment tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.UncontrolledReleaseOrSpillXPath())) {
            error = "Failed to click on the Uncontrolled Release/Spill heading";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EnvironmentPageObjects.environmentalPillGrip())) {
            error = "Failed to display Environmental Spill non editable grid  ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.UncontrolledReleaseOrSpillAddButtonXPath())) {
            error = "Failed to click on the Uncontrolled Release/Spill add button ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.EnvironmentSaveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.EnvironmentNotActiveSaveWait(), 40)) {
                error = "Webside too long to load wait reached the time out 0";
                return false;
            }
        }

        SeleniumDriverInstance.pause(1500);

//        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.environmentalSpillLoadingFormActive(), 2)) {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.environmentalSpillLoadingFormNotActive(), 40)) {
//                error = "Webside too long to load wait reached the time out 0";
//                return false;
//            }
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(EnvironmentPageObjects.evironmentSpillFlowProcessButton(), 2)) {
            error = "Failed to wait evironmental Spill Flow Process Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.evironmentSpillFlowProcessButton())) {
            error = "Failed to click evironmental Spill Flow Process Button";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EnvironmentPageObjects.evironSpillAddPhaseXpath(), 2)) {
            error = "Failed to diplay evironmental Spill Flow Process of Add phase ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Environmental Spill module opens in the Add phase successfully ");

        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.ProductDropdownXPath())) {
            error = "Failed to click on the Product dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Product")))) {
            error = "Failed to select the following item: " + getData("Product");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.UnitOFMeasureDropdownXPath())) {
            error = "Failed to click on the Unit OF Measure Dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Unit Of Measure")))) {
            error = "Failed to select the following item: " + getData("Unit Of Measure");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(EnvironmentPageObjects.DetailedDescriptionTextAreaXPath(), getData("Detailed description"))) {
            error = "Failed to enter text into the Detailed Description Text Area";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.evironSpillSaveButtonXpath())) {
            error = "Failed to click the save button";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(VerificationAndAdditionalPageObject.EnvironmentSaveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.EnvironmentNotActiveSaveWait(), 40)) {
                error = "Webside too long to load wait reached the time out 2";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(EnvironmentPageObjects.evironSpillEditPhaseXpath(), 3)) {
            error = "Failed to opens Environmental Spill module in the Add phase.";
            return false;
        }

        String submit = getData("Submit");
        String[] records = SeleniumDriverInstance.retrieveTextByXpath(EnvironmentPageObjects.envirSpillRecordNumXpath()).split("#");
        String recordNum = records[1];

        if (submit.equalsIgnoreCase("True")) {
            if (!SeleniumDriverInstance.clickElementbyXpath(EnvironmentPageObjects.SubmitButtonXPath())) {
                error = "Failed to click the Submit button";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(EnvironmentPageObjects.validateEnvirSpillgridSavedRecord(recordNum), 14)) {
                error = "Failed to save record in Incident Management edit page " + recordNum;
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully save record #" + recordNum + " Environmental Spill ");

        }

        return true;
    }

}
