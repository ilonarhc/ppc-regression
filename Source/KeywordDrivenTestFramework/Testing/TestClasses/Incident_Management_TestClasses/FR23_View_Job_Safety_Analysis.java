/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.InjuredPersonsPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.IsoMetricsIncidentMainScenarioPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.Isometrics_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_PageObjects.MainScenario_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author syotsi
 */
@KeywordAnnotation(
        Keyword = "View Job Safety Analysis",
        createNewBrowserInstance = false
)
public class FR23_View_Job_Safety_Analysis extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR23_View_Job_Safety_Analysis() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!ViewingJobSafetyAnalysis()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Passed Viewing Job Safety Analysis - Main Scenario");
    }

    public boolean ViewingJobSafetyAnalysis() {

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_RelatedJSAs_Panelxpath())) {
            error = "Failed to wait for Related JSA's.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_RelatedJSAs_Panelxpath())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_RelatedJSAs_Panelxpath())) {
                error = "Failed to wait for Related JSA's.";
                return false;
            }
            pause(8000);
            if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_RelatedJSAs_Panelxpath())) {
                error = "Failed to click Related JSA's.";
                return false;
            }
        }

        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.incidentInvestigation_RelatedJSAs_Refresh_Buttonxpath())) {
            error = "Failed to wait for Related JSA's Refresh button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(MainScenario_PageObjects.incidentInvestigation_RelatedJSAs_Refresh_Buttonxpath())) {
            error = "Failed to click Related JSA's Refresh button.";
            return false;
        }
        String retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.incidentInvestigation_RelatedJSAs_Message_Buttonxpath());
        narrator.stepPassed("Related JSA's available: " + retrieveMessage);
        pause(10000);

        return true;
    }

}
