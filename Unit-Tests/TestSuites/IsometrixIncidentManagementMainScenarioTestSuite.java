/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author RNagel
 */
public class IsometrixIncidentManagementMainScenarioTestSuite extends BaseClass {

    static TestMarshall instance;

    public IsometrixIncidentManagementMainScenarioTestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.QA;

        //*******************************************
    }

    @Test
    public void IsometrixIncidentManareleasegmentMainSc1enario() throws FileNotFoundException {
        
        Narrator.logDebug("Isometrix - Scenario 2  - Test Pack");
        instance = new TestMarshall("TestPacks\\FR6-View_Related_Obligations - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();

    }
}
